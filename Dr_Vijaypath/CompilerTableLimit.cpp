#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>


#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
#include"CompilerTableLimit.h"

bool scene_CompilerTableLimitFlag;
DrawChars *drawCharCompilerTable = NULL;
GLfloat xTranslation = 40.0f;
//Global variable declarations
int globalFlagForBox = 0;
int globalFlagForBox3 = 0;
int globalFlagForBoxForText = 0;
double a, b;
GLfloat xTranslationOfALine = 350.0f;
bool gbIsWindowsDrawn = false;
GLfloat CompilerTableFlagAfterSceneComplet = 0;
GLint noOfLinesToDraw = 24;
GLfloat xCompiler = 0;
GLfloat yCompiler = 0;
GLfloat xErrorbox = 0;
GLfloat yErrorbox = 0;
extern HWND gHwnd;
void setTextToDisplayCompilerTable(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw)
{


	drawCharCompilerTable->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
		traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

}
//----------------------------

// main display
void displayCompilerTable(void)
{
	void updateCompilerTableScale(void);
	void updateCompilerTable(void);
	void scaleText(void);

	updateCompilerTableScale();
	updateCompilerTable();
	//scaleText();

	void drawBlueScreen(void);
	void drawCompilerErrorBox(void);
	void drawErrorBox(void);



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawBlueScreen();
	drawCompilerErrorBox();
	drawErrorBox();

	// working below code
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	vector<string>  textToDraw;
	//textToDraw.resize(16);

	textToDraw.push_back("Turbo C IDE");//0
	textToDraw.push_back("VijayPath.c");
	textToDraw.push_back(" File   Edit  Search  Run   Compile  Debug   Option  Project  Window   Help");
	textToDraw.push_back("char HeterogeneousParallelProgramming");
	textToDraw.push_back("double DivisionRelationshipHoroscop");
	textToDraw.push_back("float AristarchosOfSamos");
	textToDraw.push_back("long Alchemist ");
	textToDraw.push_back("float Dodecatmoria");
	textToDraw.push_back("double Occulation");
	textToDraw.push_back("char SanguineHumour");
	textToDraw.push_back("float Sesqui-quadrate");
	textToDraw.push_back("long SpeculumOofAspects");
	textToDraw.push_back("long SublunarySphere");
	textToDraw.push_back("long ZeroPointEnergy");
	//
	if (gbIsWindowsDrawn) {
		textToDraw.push_back("Compiling"); //14  
		textToDraw.push_back("Message");//15
										//compiling box message  //line no: here starts 16
		textToDraw.push_back("Main File : Dr VijayPath.c");//16
		textToDraw.push_back("Compilingg : Dr VijayPath.c");//17
		textToDraw.push_back("Total        File"); //18
		textToDraw.push_back("Warnings:     0      0");
		textToDraw.push_back("Errrorrs:        1      1");
		textToDraw.push_back("Press Any Key");//21

											  // Error Box Message
		textToDraw.push_back("MainFileCompiling: Dr VijayPath.c");//22
		textToDraw.push_back("Error : DrVijayPath.c : COMPILER TABLE LIMIT EXCEEDS");//23
																					 //textToDraw.push_back("Error : DrVijayPath.c : COMPILER TABLE LIMIT EXCEEDS");


	}
	//double AcronichalRising


	vector<double> wordSpacVec;
	wordSpacVec.resize(24);
	wordSpacVec[0] = 2.0;
	wordSpacVec[1] = 1.0;
	wordSpacVec[2] = 2.0;
	wordSpacVec[3] = 2.0;
	wordSpacVec[4] = 2.0;
	wordSpacVec[5] = 2.0;
	wordSpacVec[6] = 2.0;
	wordSpacVec[7] = 2.0;
	wordSpacVec[8] = 2.0;
	wordSpacVec[9] = 2.0;
	wordSpacVec[10] = 2.0;
	wordSpacVec[11] = 2.0;
	wordSpacVec[12] = 2.0;
	wordSpacVec[13] = 2.0;
	//compiling..message
	wordSpacVec[14] = 2.0; //header  comiler msg
	wordSpacVec[15] = 2.0;// header error msg
						  //

	wordSpacVec[16] = 2.0;
	wordSpacVec[17] = 2.0;

	wordSpacVec[18] = 2.0;
	wordSpacVec[19] = 5.0;

	wordSpacVec[20] = 5.0;//error
	wordSpacVec[21] = 2.0;


	wordSpacVec[22] = 2.0;
	wordSpacVec[23] = 2.0;

	//wordSpacVec[24] = 2.0;


	vector<double> charSpacVec;
	charSpacVec.resize(24);

	charSpacVec[0] = 1.1;
	charSpacVec[1] = 1.1;
	charSpacVec[2] = 1.1;
	charSpacVec[3] = 1.1;
	charSpacVec[4] = 1.1;
	charSpacVec[5] = 1.1;
	charSpacVec[6] = 1.1;
	charSpacVec[7] = 1.1;
	charSpacVec[8] = 1.1;
	charSpacVec[9] = 1.1;
	charSpacVec[10] = 1.1;
	charSpacVec[11] = 1.1;
	charSpacVec[12] = 1.1;
	charSpacVec[13] = 1.1;
	charSpacVec[14] = 1.9;
	charSpacVec[15] = 1.9;

	//compiler n error msgs
	charSpacVec[16] = 1.1;
	charSpacVec[17] = 1.1;
	charSpacVec[18] = 1.1;
	charSpacVec[19] = 1.1;
	charSpacVec[20] = 1.1;
	charSpacVec[21] = 1.1;
	charSpacVec[22] = 1.1;
	charSpacVec[23] = 1.9f;
	//charSpacVec[24] = 1.9;


	vector<double> charWidth;
	charWidth.resize(24);

	charWidth[0] = 1.0;
	charWidth[1] = 1.0;
	charWidth[2] = 1.0;
	charWidth[3] = 1.0;
	charWidth[4] = 1.0;
	charWidth[5] = 1.0;
	charWidth[6] = 1.0;
	charWidth[7] = 1.0;
	charWidth[8] = 1.0;
	charWidth[9] = 1.0;
	charWidth[10] = 1.0;
	charWidth[11] = 1.0;
	charWidth[12] = 1.0;
	charWidth[13] = 1.0;
	charWidth[14] = 2.0;
	charWidth[15] = 2.0;
	//msgs
	charWidth[16] = 1.50;
	charWidth[17] = 1.50;
	charWidth[18] = 1.50;
	charWidth[19] = 1.50;
	charWidth[20] = 1.50;
	charWidth[21] = 1.50;
	charWidth[22] = 1.50;
	charWidth[23] = 3.50;
	//charWidth[24] = 1.0;




	vector<double> traslateXParam;
	traslateXParam.resize(24);
	traslateXParam[0] = -60.0f;
	traslateXParam[1] = -8.0f;
	traslateXParam[2] = -60.0f;
	traslateXParam[3] = -60.0f;
	traslateXParam[4] = -60.0f;
	traslateXParam[5] = -60.0f;
	traslateXParam[6] = -60.0f;
	traslateXParam[7] = -60.0f;
	traslateXParam[8] = -60.0f;
	traslateXParam[9] = -60.0f;
	traslateXParam[10] = -60.0f;
	traslateXParam[11] = -60.0f;
	traslateXParam[12] = -60.0f;
	traslateXParam[13] = -60.0f;
	traslateXParam[14] = -10.0f;
	traslateXParam[15] = -8.0f;

	//msgs
	traslateXParam[16] = -30.0f;
	traslateXParam[17] = -30.0f;
	traslateXParam[18] = -10.0f;
	traslateXParam[19] = -25.0f;//warning
	traslateXParam[20] = -25.0f;//error
	traslateXParam[21] = -10.0f;
	traslateXParam[22] = -60.0f;
	traslateXParam[23] = -60.0f;
	//	traslateXParam[24] = -8.0f;




	vector<double> traslateYParam;
	traslateYParam.resize(24);
	traslateYParam[0] = 33.0;
	traslateYParam[1] = 27.0;
	traslateYParam[2] = 29.3; //File edit search menu bar
	traslateYParam[3] = 18.0;
	traslateYParam[4] = 15.0;
	traslateYParam[5] = 12.0;
	traslateYParam[6] = 9.0;
	traslateYParam[7] = 6.0;
	traslateYParam[8] = 3.0;
	traslateYParam[9] = 0.0;
	traslateYParam[10] = -3.0;
	traslateYParam[11] = -6.0;
	traslateYParam[12] = -9.0;
	traslateYParam[13] = -12.0;
	traslateYParam[14] = 11.0;//Compiling
	traslateYParam[15] = -22.0;//message
							   //msg
	traslateYParam[16] = 7.0;
	traslateYParam[17] = 4.0;
	traslateYParam[18] = 1; //File edit search menu bar
	traslateYParam[19] = -2.0;
	traslateYParam[20] = -5.0;
	traslateYParam[21] = -9.0;
	traslateYParam[22] = -24.0;
	traslateYParam[23] = -28.50;
	//traslateYParam[24] = 3.0;


	vector<double> traslateZParam;
	traslateZParam.resize(24);

	traslateZParam[0] = -98.0;
	traslateZParam[1] = -98.0;
	traslateZParam[2] = -98.0;
	traslateZParam[3] = -98.0;
	traslateZParam[4] = -98.0;
	traslateZParam[5] = -98.0;
	traslateZParam[6] = -98.0;
	traslateZParam[7] = -98.0;
	traslateZParam[8] = -98.0;
	traslateZParam[9] = -98.0;
	traslateZParam[10] = -98.0;
	traslateZParam[11] = -98.0;
	traslateZParam[12] = -98.0;
	traslateZParam[13] = -98.0;
	traslateZParam[14] = -98.0;
	traslateZParam[15] = -98.0;
	//msgs
	traslateZParam[16] = -98.0;
	traslateZParam[17] = -98.0;
	traslateZParam[18] = -98.0;
	traslateZParam[19] = -98.0;
	traslateZParam[20] = -98.0;
	traslateZParam[21] = -98.0;
	traslateZParam[22] = -98.0;
	traslateZParam[23] = -98.0;
	//traslateZParam[24] = -15.0;


	vector<double> rgbVec1{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec2{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec3{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec4{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec5{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec6{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec7{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec8{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec9{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec10{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec11{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec12{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec13{ 0.0, 1.0, 0.0 };
	vector<double> rgbVec14{ 0.0, 1.0, 0.0 };

	vector<double> rgbVec15{ 1.0, 1.0, 0.0 };
	vector<double> rgbVec16{ 1.0, 1.0, 0.0 };
	//msgs
	vector<double> rgbVec17{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec18{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec19{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec20{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec21{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec22{ 0.0, 0.0, 0.0 };
	vector<double> rgbVec23{ 0.0, 0.0, 1.0 };
	vector<double> rgbVec24{ 1.0, 0.0, 0.0 };
	//vector<double> rgbVec25{ 0.0, 1.0, 0.0 };



	vector<vector<double>>rgbValueVec;
	rgbValueVec.resize(24);
	rgbValueVec[0] = rgbVec1;
	rgbValueVec[1] = rgbVec2;
	rgbValueVec[2] = rgbVec3;
	rgbValueVec[3] = rgbVec4;
	rgbValueVec[4] = rgbVec5;
	rgbValueVec[5] = rgbVec6;
	rgbValueVec[6] = rgbVec7;
	rgbValueVec[7] = rgbVec8;
	rgbValueVec[8] = rgbVec9;
	rgbValueVec[9] = rgbVec10;
	rgbValueVec[10] = rgbVec11;
	rgbValueVec[11] = rgbVec12;
	rgbValueVec[12] = rgbVec13;
	rgbValueVec[13] = rgbVec14;
	rgbValueVec[14] = rgbVec15;
	rgbValueVec[15] = rgbVec16;


	rgbValueVec[16] = rgbVec17;
	rgbValueVec[17] = rgbVec18;
	rgbValueVec[18] = rgbVec19;
	rgbValueVec[19] = rgbVec20;
	rgbValueVec[20] = rgbVec21;
	rgbValueVec[21] = rgbVec22;
	rgbValueVec[22] = rgbVec23;
	rgbValueVec[23] = rgbVec24;
	//rgbValueVec[24] = rgbVec9;
	//	rgbValueVec[25] = rgbVec10;

	vector<double> scaleVec1{ 0.1f, 0.1f, 0.0f };
	vector<double> scaleVec2{ 0.1f, 0.1f, 0.0f };
	vector<double> scaleVec3{ 0.1f, 0.1f, 0.0f };
	vector<double> scaleVec4{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec5{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec6{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec7{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec8{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec9{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec10{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec11{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec12{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec13{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec14{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec15{ 0.3f, 0.3f, 0.0f };
	vector<double> scaleVec16{ 0.3f, 0.3f, 0.0f };

	//msgs
	vector<double> scaleVec17{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec18{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec19{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec20{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec21{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec22{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec23{ 0.2f, 0.2f, 0.0f };
	vector<double> scaleVec24{ 0.3f, 0.3f, 0.0f };
	//vector<double> scaleVec24{ xScaleText, yScaleText, 0.0f };

	//vector<double> scaleVec25{ 0.2f, 0.2f, 0.0f };

	vector<vector<double>>scaleValueVec;
	scaleValueVec.resize(24);
	scaleValueVec[0] = scaleVec1;
	scaleValueVec[1] = scaleVec2;
	scaleValueVec[2] = scaleVec3;
	scaleValueVec[3] = scaleVec4;
	scaleValueVec[4] = scaleVec5;
	scaleValueVec[5] = scaleVec6;
	scaleValueVec[6] = scaleVec7;
	scaleValueVec[7] = scaleVec8;
	scaleValueVec[8] = scaleVec9;
	scaleValueVec[9] = scaleVec10;
	scaleValueVec[10] = scaleVec11;
	scaleValueVec[11] = scaleVec12;
	scaleValueVec[12] = scaleVec13;
	scaleValueVec[13] = scaleVec14;
	scaleValueVec[14] = scaleVec15;
	scaleValueVec[15] = scaleVec16;


	//msgs
	scaleValueVec[16] = scaleVec17;
	scaleValueVec[17] = scaleVec18;
	scaleValueVec[18] = scaleVec19;
	scaleValueVec[19] = scaleVec20;
	scaleValueVec[20] = scaleVec21;
	scaleValueVec[21] = scaleVec22;
	scaleValueVec[22] = scaleVec23;
	scaleValueVec[23] = scaleVec24;

	//setTextToDisplayCompilerTable(wordSpacVec, harSpac, textToDisplaycharWidth, textToDisplayTranslateXParam, textToDisplayTranslateYParam, textToDisplayTranslateZParam, textToDisplayRGBValueVec, textToDisplayScaleValueVec, textToDisplayString);
	setTextToDisplayCompilerTable(wordSpacVec, charSpacVec, charWidth, traslateXParam, traslateYParam, traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

	drawCharCompilerTable->setWordToDisplayOnScreen();

	glPopMatrix();



	//}

	//if (globalFlagForBox = 2)
	//	{
	//	

	//drawBlueScreen();

	//	}

}


void updateCompilerTable(void)
{

	xTranslationOfALine = xTranslationOfALine - 0.01f;
	if (xTranslationOfALine >= 160.0f)
		xTranslationOfALine = 80;

	//if (xTranslationOfALine <= 80.0f && xTranslationOfALine >= -60.0f)
	//xTranslationOfALine = -60;
}
//**

void drawText(void)
{

	//vector<GLfloat> traslateVec;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


}

void updateCompilerTableScale(void)
{
	a = a + 0.1f;
	if (a >= 67.5f)
	{
		a = 67.5f;
		gbIsWindowsDrawn = true;
		globalFlagForBox = 1;
		globalFlagForBoxForText = 1;

	}

	b = b + 0.1f;
	if (b >= 36.5f)
	{
		b = 36.5f;

	}

	//glScalef(66.5f, 37.5f
	//for compiler box
	if (globalFlagForBox == 1)
	{
		xCompiler = xCompiler + 0.1f;
		if (xCompiler >= 66.5)
			xCompiler = 66.5;

		yCompiler = yCompiler + 0.1f;
		if (yCompiler >= 37.5)
		{
			yCompiler = 37.5;
			//	
		}

		// updateCompilerTable for error box flag3 set to 2
		//alef(66.5f, 37.5f
		if (xCompiler >= 66.5f)
		{
			globalFlagForBox3 = 2;
		}
	}

}
void disableCompilerTableLimitFlag() {
	scene_CompilerTableLimitFlag = true;
	KillTimer(gHwnd, 44);
}
void initializeCompilerTable(void) {
	drawCharCompilerTable = new DrawChars();
	SetTimer(gHwnd, 44, 10000, (TIMERPROC)disableCompilerTableLimitFlag);
}

void uninitializeCompilerTable(void) {
	delete drawCharCompilerTable;
}

void drawBlueScreen(void)
{
	glPushMatrix();
	//glTranslatef(0.0f, 0.0f, -1.5f);
	//glScalef(6.5f, 3.6f, 1.0f);


	glTranslatef(0.0f, 0.0f, -98.5f);
	glScalef(a, b, 1.0f);

	glBegin(GL_QUADS);
	//outside box
	glColor3f(0.0f, 0.0f, 1.0f); //BLUE
	glVertex3f(1.0f, 1.0f, 0.0f); //right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 0.0f); //left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 0.0f); //left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 0.0f); //right-bottom corner of front face
	glEnd();
	glBegin(GL_QUADS);
	//Top-  bar..before menu
	glColor3f(0.2, 0.4, 1); //BLUE
	glVertex3f(1.0f, 1.0f, 0.0f); //right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 0.0f); //left-top corner of front face
	glVertex3f(-1.0f, 0.9f, 0.0f); //left-bottom corner of front face
	glVertex3f(1.0f, 0.9f, 0.0f); //right-bottom corner of front face
	glEnd();

	glBegin(GL_QUADS);
	//Top-FILE.. menu bar
	glColor3f(0.850f, 0.850f, 0.850f); //BLUE
	glVertex3f(1.0f, 0.9f, 0.0f); //right-top corner of front face
	glVertex3f(-1.0f, 0.9f, 0.0f); //left-top corner of front face
	glVertex3f(-1.0f, 0.8f, 0.0f); //left-bottom corner of front face
	glVertex3f(1.0f, 0.8f, 0.0f); //right-bottom corner of front face
	glEnd();

	glBegin(GL_LINES);
	// 3 lines before FILE menu
	glColor3f(1.0f, 0.0f, 0.0f); //BLUE
	glVertex3f(-0.98f, 0.85f, 0.0f); //right-top corner of front face
	glVertex3f(-0.95f, 0.85f, 0.0f);

	glVertex3f(-0.98f, 0.865f, 0.0f); //right-top corner of front face
	glVertex3f(-0.95f, 0.865f, 0.0f);

	glVertex3f(-0.98f, 0.83f, 0.0f); //right-top corner of front face
	glVertex3f(-0.95f, 0.83f, 0.0f);
	glEnd();


	glBegin(GL_QUADS);
	//Top-second border GREEN BOX
	glColor3f(0.2f, 1.0f, 0.0f); //
	glVertex3f(-0.845f, 0.79f, 0.0f); //r
	glVertex3f(-0.86f, 0.79f, 0.0f); //
	glVertex3f(-0.86f, 0.75f, 0.0f); //
	glVertex3f(-0.845f, 0.75f, 0.0f); //
	glEnd();
	glBegin(GL_LINES);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-0.885f, 0.79f, 0.0f); //r
	glVertex3f(-0.885f, 0.745f, 0.0f); //


	glVertex3f(-0.82f, 0.79f, 0.0f); //r
	glVertex3f(-0.82f, 0.745f, 0.0f); //


	glEnd();
	//end of box

	glBegin(GL_LINES);
	//2 white border line 
	glColor3f(1.0f, 1.0f, 1.0f);
	//top first line 
	glVertex3f(-0.98f, 0.78f, 0.0f);
	glVertex3f(-0.89f, 0.78f, 0.0f);
	glVertex3f(-0.81f, 0.78f, 0.0f);
	glVertex3f(-0.15f, 0.78f, 0.0f);
	glVertex3f(0.98f, 0.78f, 0.0f);
	glVertex3f(0.15f, 0.78f, 0.0f);

	//top second line
	glVertex3f(-0.96f, 0.76f, 0.0f);
	glVertex3f(-0.89f, 0.76f, 0.0f);
	glVertex3f(-0.81f, 0.76f, 0.0f);
	glVertex3f(-0.15f, 0.76f, 0.0f);
	glVertex3f(0.96f, 0.76f, 0.0f);
	glVertex3f(0.15f, 0.76f, 0.0f);
	//up 2 lines complete


	//bottom two line
	glVertex3f(-0.98f, -0.98f, 0.0f);
	glVertex3f(-0.89f, -0.98f, 0.0f);
	glVertex3f(-0.81f, -0.98f, 0.0f);
	glVertex3f(0.98f, -0.98f, 0.0f);

	glVertex3f(-0.96f, -0.96f, 0.0f);
	glVertex3f(-0.89f, -0.96f, 0.0f);
	glVertex3f(-0.81f, -0.96f, 0.0f);
	glVertex3f(0.96f, -0.96f, 0.0f);

	//1:1

	glVertex3f(-0.87f, -0.988f, 0.0f); //1:1
	glVertex3f(-0.87f, -0.95f, 0.0f); //

	glVertex3f(-0.84f, -0.988f, 0.0f); //r1:1
	glVertex3f(-0.84f, -0.95f, 0.0f); //

	glVertex3f(-0.856f, -0.97f, 0.0f); //r1:1
	glVertex3f(-0.859f, -0.97f, 0.0f); //
	glVertex3f(-0.856f, -0.98f, 0.0f); //r1:1
	glVertex3f(-0.859f, -0.98f, 0.0f); //


									   //bottom line complete

									   //left verticle
	glVertex3f(-0.98f, 0.78f, 0.0f);
	glVertex3f(-0.98f, -0.98f, 0.0f);
	glVertex3f(-0.96f, 0.76f, 0.0f);
	glVertex3f(-0.96f, -0.96f, 0.0f);

	glVertex3f(0.98f, 0.78f, 0.0f); //right verticle
	glVertex3f(0.98f, -0.98f, 0.0f);
	glVertex3f(0.96f, 0.76f, 0.0f); //right
	glVertex3f(0.96f, -0.96f, 0.0f);
	//left right border completes

	glEnd();
	glPopMatrix();

	//dispalyTextOnBlueScreen();

}



void drawCompilerErrorBox(void)
{

	if (globalFlagForBox == 1)
	{

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -98.5f);
		//glScalef(64.5f, 40.5f, 1.0f);
		glScalef(xCompiler, yCompiler, 1.0f);
		glBegin(GL_QUADS);
		//Top-second border GREEN BOX
		glColor3f(0.75f, 0.75f, 0.75f); //
		glVertex3f(-0.5f, 0.4f, 0.0f); //r
		glVertex3f(0.5f, 0.4f, 0.0f); //
		glVertex3f(0.5f, -0.4f, 0.0f); //
		glVertex3f(-0.5f, -0.4f, 0.0f); //
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.48f, 0.38f, 0.0f);
		glVertex3f(-0.15f, 0.38f, 0.0f);

		glVertex3f(0.15f, 0.38f, 0.0f);
		glVertex3f(0.48f, 0.38f, 0.0f);

		glVertex3f(-0.46f, 0.36f, 0.0f);
		glVertex3f(-0.15f, 0.36f, 0.0f);

		glVertex3f(0.15f, 0.36f, 0.0f);
		glVertex3f(0.46f, 0.36f, 0.0f);


		glVertex3f(-0.48f, -0.38f, 0.0f);
		glVertex3f(0.48f, -0.38f, 0.0f);

		glVertex3f(-0.46f, -0.36f, 0.0f);
		glVertex3f(0.46f, -0.36f, 0.0f);

		//verticl line for error box
		glVertex3f(-0.48f, 0.38f, 0.0f);
		glVertex3f(-0.48f, -0.38f, 0.0f);
		glVertex3f(-0.46f, 0.36f, 0.0f);
		glVertex3f(-0.46f, -0.36f, 0.0f);

		glVertex3f(0.48f, 0.38f, 0.0f);
		glVertex3f(0.48f, -0.38f, 0.0f);
		glVertex3f(0.46f, 0.36f, 0.0f);
		glVertex3f(0.46f, -0.36f, 0.0f);


		glEnd();
		//		dispalyTextOnCompilerErrorBox();

		globalFlagForBox = 2;
		glPopMatrix();

	}

}



void drawErrorBox(void)
{

	if (globalFlagForBox3 == 2)
	{
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -98.5f);
		glScalef(66.5f, 37.5f, 1.0f);
		//glScalef(xErrorbox, yErrorbox, 1.0f);

		//	glTranslatef(0.0f, 0.0f, -1.5f);
		//	glScalef(6.4f, 3.7f, 1.0f);
		glBegin(GL_QUADS);
		//Top-FILE.. menu bar
		glColor3f(0.0f, 0.80f, 0.80f); //BLUE
		glVertex3f(-1.0f, -0.5f, 0.0f); //right-top corner of front face
		glVertex3f(1.0f, -0.5f, 0.0f); //left-top corner of front face
		glVertex3f(1.0f, -1.0f, 0.0f); //left-bottom corner of front face
		glVertex3f(-1.0f, -1.0f, 0.0f); //right-bottom corner of front face
		glEnd();


		glBegin(GL_QUADS);
		//Top-second border GREEN BOX
		glColor3f(0.0f, 1.0f, 0.0f); //
		glVertex3f(-0.845f, -0.51f, 0.0f); //r
		glVertex3f(-0.86f, -0.51f, 0.0f); //
		glVertex3f(-0.86f, -0.55f, 0.0f); //
		glVertex3f(-0.845f, -0.55f, 0.0f); //
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.87f, -0.51f, 0.0f); //r
		glVertex3f(-0.87f, -0.55f, 0.0f); //


		glVertex3f(-0.83f, -0.51f, 0.0f); //r
		glVertex3f(-0.83f, -0.55f, 0.0f); //


		glEnd();
		//=========================================

		glBegin(GL_LINES);
		//2 white border line 
		glColor3f(1.0f, 1.0f, 1.0f);
		//top first line 
		glVertex3f(-0.98f, -0.51f, 0.0f);
		glVertex3f(-0.89f, -0.51f, 0.0f);
		glVertex3f(-0.81f, -0.51f, 0.0f);
		glVertex3f(-0.15f, -0.51f, 0.0f);
		glVertex3f(0.98f, -0.51f, 0.0f);
		glVertex3f(0.15f, -0.51f, 0.0f);

		//top second line
		glVertex3f(-0.96f, -0.53f, 0.0f);
		glVertex3f(-0.89f, -0.53f, 0.0f);
		glVertex3f(-0.81f, -0.53f, 0.0f);
		glVertex3f(-0.15f, -0.53f, 0.0f);
		glVertex3f(0.96f, -0.53f, 0.0f);
		glVertex3f(0.15f, -0.53f, 0.0f);
		//up 2 lines complete


		//bottom two line
		glVertex3f(-0.98f, -0.98f, 0.0f);
		glVertex3f(-0.89f, -0.98f, 0.0f);
		glVertex3f(-0.81f, -0.98f, 0.0f);
		glVertex3f(0.98f, -0.98f, 0.0f);

		glVertex3f(-0.96f, -0.96f, 0.0f);
		glVertex3f(-0.89f, -0.96f, 0.0f);
		glVertex3f(-0.81f, -0.96f, 0.0f);
		glVertex3f(0.96f, -0.96f, 0.0f);

		//1:1

		glVertex3f(-0.87f, -0.988f, 0.0f); //1:1
		glVertex3f(-0.87f, -0.95f, 0.0f); //

		glVertex3f(-0.84f, -0.988f, 0.0f); //r1:1
		glVertex3f(-0.84f, -0.95f, 0.0f); //

		glVertex3f(-0.856f, -0.97f, 0.0f); //r1:1
		glVertex3f(-0.859f, -0.97f, 0.0f); //
		glVertex3f(-0.856f, -0.98f, 0.0f); //r1:1
		glVertex3f(-0.859f, -0.98f, 0.0f); //


										   //bottom line complete

										   //left verticle
		glVertex3f(-0.98f, -0.51f, 0.0f);
		glVertex3f(-0.98f, -0.98f, 0.0f);
		glVertex3f(-0.96f, -0.53f, 0.0f);
		glVertex3f(-0.96f, -0.96f, 0.0f);

		glVertex3f(0.98f, -0.51f, 0.0f); //right verticle
		glVertex3f(0.98f, -0.98f, 0.0f);
		glVertex3f(0.96f, -0.53f, 0.0f); //right
		glVertex3f(0.96f, -0.96f, 0.0f);
		//left right border completes

		glEnd();
		glPopMatrix();
		//dispalyTextOnErrorBox();

		CompilerTableFlagAfterSceneComplet = 1;

	}
}
