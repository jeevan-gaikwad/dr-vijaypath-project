#pragma once
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
using namespace std;

void KnRUpdate();
void scene_KnRDisplay(void);
void scene_KnRInitialize(void);
void setTextToDisplayKnR();
void drawBigCKnR();
void drawSingleHzLineKnR(vector<GLfloat> rgbVals, GLfloat wdth, GLfloat yValue);
void drawHorizontalLinesKnR();
void drawRectangleKnR();
void uninitializeKnR(void);