#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include "Resources.h"
#include"DrawChars.h"

#define WIN_WIDTH 1920
#define WIN_HEIGHT 1080

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

GLUquadric *quadric = NULL;
GLfloat lookatz = 0.0f;
GLfloat lookatz2 = 0.0f;
GLfloat angleMercury = 120.0f;
GLfloat angleVenus = 90.0f;
GLfloat angleEarth = 90.0f;
GLfloat angleMars = 90.0f;
GLfloat angleJupiter = 180.0f;
GLfloat angleSaturn = 10.0f;
GLfloat angleUranus = -180.0f;
GLfloat angleNeptune = 45.0f;
GLfloat anglePluto = -180.0f;
GLfloat KundaliPlanetSpeed = 0.007f;
GLfloat KundaliXspeed = 0.0012f;
GLfloat KundaliYspeed = 0.0012f;

GLuint Texture_Sun;
GLuint Texture_Mercury;
GLuint	Texture_Venus;
GLuint	Texture_Earth;
GLuint	Texture_Mars;
GLuint	Texture_Jupiter;
GLuint	Texture_Saturn;
GLuint	Texture_Uranus;
GLuint	Texture_Neptune;
GLuint	Texture_Pluto;
GLuint	Texture_Moon;

GLfloat Saturnx = 0.0f;
GLfloat Saturny = 0.0f;
GLfloat Saturnz = 0.0f;

GLfloat Venusx = 0.0f;
GLfloat Venusy = 0.0f;
GLfloat Venusz = -1000.0f;

GLfloat Jupiterx = 0.0f;
GLfloat Jupitery = 0.0f;
GLfloat Jupiterz = -1000.0f;

GLfloat Plutox = 0.0f;
GLfloat Plutoy = 0.0f;
GLfloat Plutoz = -1000.0f;

GLfloat Sunx = 0.0f;
GLfloat Suny = 0.0f;
GLfloat Sunz = -1000.0f;

GLfloat Uranusx = 0.0f;
GLfloat Uranusy = 0.0f;
GLfloat Uranusz = -1000.0f;

GLfloat Marsx = 0.0f;
GLfloat Marsy = 0.0f;
GLfloat Marsz = -1000.0f;

GLfloat Mercuryx = 0.0f;
GLfloat Mercuryy = 0.0f;
GLfloat Mercuryz = -1000.0f;

GLfloat Neptunex = 0.0f;
GLfloat Neptuney = 0.0f;
GLfloat Neptunez = -1000.0f;

GLfloat Moonx = 0.0f;
GLfloat Moony = 0.0f;
GLfloat Moonz = -1000.0f;

GLfloat KundaliLinesColorr = 0.0f;
GLfloat KundaliLinesColorg = 0.0f;
GLfloat KundaliLinesColorb = 0.0f;

GLfloat angle = 0.0f;

GLfloat drawKundaliLines = 0.0f; // to draw , value should be 1
GLint Scene = 1;
GLint LetterAstromedicomp = 0;
GLint Iteration = 0;  // to change scene
GLint Scene3timer = 0;

bool Scene_SolarSystemAnimation = true;

//Function declaration
void solarSystem(void);
void update(void);
void Kundali(void);
void KundaliLines(void);
void KundaliSaturn(void);
void KundaliVenus(void);
void KundaliJupiter(void);
void KundaliPluto(void);
void KundaliSun(void);
void KundaliUranus(void);
void KundaliMars(void);
void KundaliMercury(void);
void KundaliNeptune(void);
void KundaliMoon(void);
void KundaliKetu(void);
void KundaliRahu(void);
void KundaliAnimation(void);


static int year = 0;
static int day = 0;
DrawChars *drawChar = NULL;


void solarSystem_initialize(void)
{
	glDisable(GL_LIGHTING);
    int LoadGLTextures_Solar(GLuint *, TCHAR[]);
	LoadGLTextures_Solar(&Texture_Sun, MAKEINTRESOURCE(IDBITMAP_SUN));
	LoadGLTextures_Solar(&Texture_Mercury, MAKEINTRESOURCE(IDBITMAP_MERCURY));
	LoadGLTextures_Solar(&Texture_Venus, MAKEINTRESOURCE(IDBITMAP_VENUS));
	LoadGLTextures_Solar(&Texture_Earth, MAKEINTRESOURCE(IDBITMAP_EARTH));
	LoadGLTextures_Solar(&Texture_Mars, MAKEINTRESOURCE(IDBITMAP_MARS));
	LoadGLTextures_Solar(&Texture_Jupiter, MAKEINTRESOURCE(IDBITMAP_JUPITER));
	LoadGLTextures_Solar(&Texture_Saturn, MAKEINTRESOURCE(IDBITMAP_SATURN));
	LoadGLTextures_Solar(&Texture_Uranus, MAKEINTRESOURCE(IDBITMAP_URANUS));
	LoadGLTextures_Solar(&Texture_Neptune, MAKEINTRESOURCE(IDBITMAP_NEPTUNE));
	LoadGLTextures_Solar(&Texture_Pluto, MAKEINTRESOURCE(IDBITMAP_PLUTO));
	LoadGLTextures_Solar(&Texture_Moon, MAKEINTRESOURCE(IDBITMAP_MOON));

	drawChar = new DrawChars();
	PlaySound(TEXT("SolarSystem\\Interstellar_main_theme.wav"), NULL, SND_FILENAME | SND_ASYNC);
}

void setTextToDisplay(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw)
{

	
	drawChar->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
		traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

}
void drawASTROMEDICOMP() {
	vector<string>  textToDraw;
	textToDraw.resize(1);
	textToDraw[0] = "ASTROMEDICOMP";
	vector<double> wordSpacVec;
	wordSpacVec.resize(1);
	wordSpacVec[0] = 0.0;

	vector<double> charSpacVec;
	charSpacVec.resize(1);

	charSpacVec[0] = 2.0;

	vector<double> charWdth;
	charWdth.resize(1);

	charWdth[0] = 8.0f;

	vector<double> traslateXParam;
	traslateXParam.resize(1);

	traslateXParam[0] = -15.0f;

	vector<double> traslateYParam;
	traslateYParam.resize(1);

	traslateYParam[0] = 11.5f;

	vector<double> traslateZParam;
	traslateZParam.resize(1);

	traslateZParam[0] = -40.0f;


	vector<double> rgbVec1{ 0.6, 0.6, 0.0 };
	vector<vector<double>>rgbValueVec;
	rgbValueVec.resize(1);
	rgbValueVec[0] = rgbVec1;

	vector<vector<double>>scaleValueVec;
	scaleValueVec.resize(9);
	vector<double> scaleVec1{ 0.2f, 0.2f, 1.0f };
	scaleValueVec[0] = scaleVec1;
	setTextToDisplay(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam, traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

	drawChar->setWordToDisplayOnScreen();

}
void solarSystem_display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// View transformation
	

	if (Scene == 1)
	{
		gluLookAt(0.0f, 0.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // front view
		//KundaliSaturnAnimation();


		 //gluLookAt(0.0f, 90.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f); // top view

		//gluLookAt(0.0f, 0.0f, 18.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // Kundali View

		update();
		solarSystem();
	}
	else if (Scene == 2)
	{
		gluLookAt(0.0f, 0.0f, lookatz2, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // front view
			
		

		update();
		solarSystem();
	}

	else if (Scene == 3)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		Kundali();
	}
}

int LoadGLTextures_Solar(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//generate mipmapped texture (3 bytes, width, height & data from bmp)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}


void solarSystem(void)
{

	// Sun
		glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, Texture_Sun);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 2.0, 100, 100);
	glPopMatrix();

	//Mercury
	glBindTexture(GL_TEXTURE_2D, Texture_Mercury);
	glRotatef(angleMercury, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 3.0f); // merucy radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.3f, 0.3f, 0.3f);
	gluSphere(quadric, 0.2f, 100, 100);
	glPopMatrix();

	//Venus
	glBindTexture(GL_TEXTURE_2D, Texture_Venus);
	glRotatef(angleVenus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 4.0f); // Venus radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.8f, 0.2f);
	gluSphere(quadric, 0.5f, 100, 100);
	glPopMatrix();

	//Earth
	glBindTexture(GL_TEXTURE_2D, Texture_Earth);
	glRotatef(angleEarth, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 7.0f); // Earth radius from Sun
	glPushMatrix();
	glRotatef(120, -1.0f, -1.0f, -0.5f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.0f, 1.0f);
	gluSphere(quadric, 0.8f, 100, 100);
	glPopMatrix();

	//Mars
	glBindTexture(GL_TEXTURE_2D, Texture_Mars);
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 9.0f); // Mars radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.0f);
	gluSphere(quadric, 0.5f, 100, 100);
	glPopMatrix();

	//Jupiter
	glBindTexture(GL_TEXTURE_2D, Texture_Jupiter);
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 12.0f); // Jupiter radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.6f, 0.5f);
	gluSphere(quadric, 0.9f, 100, 100);
	glPopMatrix();

	//Saturn
	glBindTexture(GL_TEXTURE_2D, Texture_Saturn);
	glRotatef(angleSaturn, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 15.0f); // Saturn radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 1.0f, 0.5f);
	gluSphere(quadric, 0.8f, 100, 100);
	glPopMatrix();

	glPushMatrix();

	// saturn ring
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glRotatef(70.0f, -1.2f, -0.3f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.20, 1.30, 100, 1, 0, 360); //inner ring
	glColor3f(0.4f, 0.4f, 0.4f);
	gluPartialDisk(quadric, 1.30, 1.35, 100, 1, 0, 360); // middle ring 
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.35, 1.45, 100, 1, 0, 360); //outer ring

	glPopMatrix();

	//Uranus
	glBindTexture(GL_TEXTURE_2D, Texture_Uranus);
	glRotatef(angleUranus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 18.0f); // Uranus radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.5f, 1.0f);
	gluSphere(quadric, 0.6f, 100, 100);
	glPopMatrix();

	//Neptune
	glBindTexture(GL_TEXTURE_2D, Texture_Neptune);
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 20.0f); // Neptune radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.3f, 1.0f);
	gluSphere(quadric, 0.5f, 100, 100);
	glPopMatrix();

	//Pluto
	glBindTexture(GL_TEXTURE_2D, Texture_Pluto);
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 22.0f); // Pluto radius from Sun
	glPushMatrix();
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.2f);
	gluSphere(quadric, 0.3f, 100, 100);
	glPopMatrix();


}

void update(void)
{
	// Lookat transformation

	lookatz = lookatz + 0.0025;


	if (lookatz >= 18)
	{
		lookatz = lookatz + 0.08;
	}

	if (lookatz >= 120)
	{
		Scene = 3;

	}



	//mercury rotation
	angleMercury = angleMercury + 0.030;
	if (angleMercury >= 315)
		angleMercury = -45;

	//Venus rotation
	angleVenus = angleVenus + 0.0005;
	if (angleVenus >= 315)
		angleVenus = -45;

	//Earth rotation
	angleEarth = angleEarth + 0.0005;
	if (angleEarth >= 360)
		angleEarth = 0;

	//Mars rotation
	angleMars = angleMars + 0.0009;
	if (angleMars >= 360)
		angleMars = 0;

	// Saturn rotation
	angleSaturn = angleSaturn + 0.000005;
	if (angleSaturn >= 360)
		angleSaturn = 0;

	// Uranus rotation
	angleUranus = angleUranus + 0.000009;
	if (angleUranus >= 360)
		angleUranus = 0;

	// Neptune rotation
	angleNeptune = angleNeptune + 0.0000005;
	if (angleNeptune >= 360)
		angleNeptune = 0;

	// Pluto rotation
	anglePluto = anglePluto + 0.0000009;
	if (anglePluto >= 360)
		anglePluto = 0;

}

void Kundali(void)
{
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliSaturn();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliVenus();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliJupiter();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliPluto();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliSun();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliUranus();
	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliMars();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliMercury();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliNeptune();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliMoon();

	glDisable(GL_TEXTURE_2D);

	
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliLines();
	
	if (LetterAstromedicomp == 1)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		KundaliKetu();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		KundaliRahu();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		drawASTROMEDICOMP();
	}

	glEnable(GL_TEXTURE_2D);

	KundaliAnimation();
}

void KundaliLines(void)
{
	glTranslatef(0.0f, 0.0f, -18.0f);
	glLineWidth(8);
	glBegin(GL_LINES);
	
	//glColor3f(0.0f, 0.3f, 0.6f);  // Line colors Blue
	glColor3f(KundaliLinesColorr, KundaliLinesColorg, KundaliLinesColorb);
	
	glVertex3f(8.0f, 5.0f, 0.0f); //1
	glVertex3f(0.0f, 5.0f, 0.0f); //2

	glVertex3f(0.0f, 5.0f, 0.0f); //2
	glVertex3f(-8.0f, 5.0f, 0.0f); //3

	glVertex3f(-8.0, 5.0f, 0.0f); //3
	glVertex3f(-8.0f, 0.0f, 0.0f); //4

	glVertex3f(-8.0f, 0.0f, 0.0f); //4
	glVertex3f(-8.0f, -5.0f, 0.0f); //5

	glVertex3f(-8.0f, -5.0f, 0.0f); //5
	glVertex3f(0.0f, -5.0f, 0.0f); //6

	glVertex3f(0.0f, -5.0f, 0.0f); //6
	glVertex3f(8.0f, -5.0f, 0.0f); //7

	glVertex3f(8.0f, -5.0f, 0.0f); //7
	glVertex3f(8.0f, 0.0f, 0.0f); //8

	glVertex3f(8.0f, 0.0f, 0.0f); //8
	glVertex3f(8.0f, 5.0f, 0.0f); //1

	glVertex3f(8.0f, 0.0f, 0.0f); //8
	glVertex3f(0.0f, 5.0f, 0.0f); //2

	glVertex3f(0.0f, 5.0f, 0.0f); //2
	glVertex3f(-8.0f, 0.0f, 0.0f); //4

	glVertex3f(-8.0f, 0.0f, 0.0f); //4
	glVertex3f(0.0f, -5.0f, 0.0f); //6

	glVertex3f(0.0f, -5.0f, 0.0f); //6
	glVertex3f(8.0f, 0.0f, 0.0f); //8

	glVertex3f(8.0f, 5.0f, 0.0f); //1
	glVertex3f(-8.0f, -5.0f, 0.0f); //5

	glVertex3f(-8.0f, 5.0f, 0.0f); //3
	glVertex3f(8.0f, -5.0f, 0.0f); //7

	// Upper square of Kundali (ASTROMEDICOMP)
	glVertex3f(8.0f, 6.5f, 0.0f); //9
	glVertex3f(-8.0, 6.5f, 0.0f); //10

	glVertex3f(-8.0, 6.5f, 0.0f); //10
	glVertex3f(-8.0, 5.2f, 0.0f); //11

	glVertex3f(-8.0, 5.2f, 0.0f); //11
	glVertex3f(8.0, 5.2f, 0.0f); //12
	
	glVertex3f(8.0, 5.2f, 0.0f); //12
	glVertex3f(8.0f, 6.5f, 0.0f); //9


	glEnd();
}

void KundaliSaturn(void)
{
	glColor3f(1.0f, 1.0f, 1.0f);
	//Saturn
	glBindTexture(GL_TEXTURE_2D, Texture_Saturn);
	//glRotatef(angleSaturn, 0.0f, 1.0f, 0.0f);
	//glTranslatef(3.3f, -3.1f, Saturnz); 
	glTranslatef(Saturnx, Saturny, Saturnz);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 1.0f, 0.5f);
	gluSphere(quadric, 0.5f, 100, 100);
	glPopMatrix();
	glPushMatrix();

	// saturn ring
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glRotatef(80.0f, -1.2f, -0.2f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 0.60, 0.70, 100, 1, 0, 360); //inner ring
	glColor3f(0.4f, 0.4f, 0.4f);
	gluPartialDisk(quadric, 0.70, 0.75, 100, 1, 0, 360); // middle ring 
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 0.75, 0.85, 100, 1, 0, 360); //outer ring

	glPopMatrix();
}


void KundaliVenus(void)
{
	//Venus
	glBindTexture(GL_TEXTURE_2D, Texture_Venus);
	//glRotatef(angleVenus, 0.0f, 1.0f, 0.0f);
	//glTranslatef(1.5f, 4.0f, -25.0f); 
	glTranslatef(Venusx, Venusy, Venusz);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.8f, 0.2f);
	gluSphere(quadric, 0.35f, 100, 100);

}

void KundaliJupiter(void)
{
	//Jupiter
	glBindTexture(GL_TEXTURE_2D, Texture_Jupiter);
	
	//glTranslatef(-1.5, 4.0f, -25.0f); 
	glTranslatef(Jupiterx, Jupitery, Jupiterz);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.6f, 0.5f);
	gluSphere(quadric, 0.4f, 100, 100);

}

void KundaliPluto(void)
{
	//Pluto
	glBindTexture(GL_TEXTURE_2D, Texture_Pluto);
	//glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	//glTranslatef(0.0, 2.0f, -30.0f); 
	glTranslatef(Plutox, Plutoy, Plutoz);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.2f);
	gluSphere(quadric, 0.2f, 100, 100);


}

void KundaliSun(void)
{
	//Sun
	glBindTexture(GL_TEXTURE_2D, Texture_Sun);
	glTranslatef(Sunx, Suny, Sunz);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28f, 100, 100);
}

void KundaliUranus(void)
{
	//Uranus
	glBindTexture(GL_TEXTURE_2D, Texture_Uranus);
	//glRotatef(angleUranus, 0.0f, 1.0f, 0.0f);
	glTranslatef(Uranusx, Uranusy, Uranusz);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.5f, 1.0f);
	gluSphere(quadric, 0.4f, 100, 100);

}

void KundaliMars(void)
{
	//Mars
	glBindTexture(GL_TEXTURE_2D, Texture_Mars);
	//glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(Marsx, Marsy, Marsz);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f); // Rotate planet
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.0f);
	gluSphere(quadric, 0.8f, 100, 100);

}

void KundaliMercury()
{
	//Mercury
	glBindTexture(GL_TEXTURE_2D, Texture_Mercury);
	//glRotatef(angleMercury, 0.0f, 1.0f, 0.0f);
	glTranslatef(Mercuryx, Mercuryy, Mercuryz);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.3f, 0.3f, 0.3f);
	gluSphere(quadric, 0.3f, 100, 100);

}

void KundaliNeptune()
{
	//Neptune
	glBindTexture(GL_TEXTURE_2D, Texture_Neptune);
	//glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(Neptunex, Neptuney, Neptunez);
	glRotatef(90.0f, 1.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.3f, 1.0f);
	gluSphere(quadric, 0.4f, 100, 100);


}

void KundaliMoon()
{
	//Moon
	glBindTexture(GL_TEXTURE_2D, Texture_Moon);
	//glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(Moonx, Moony, Moonz);
	glRotatef(170.0f, 1.0f, 1.0f, 0.0f);
	//glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.3f, 1.0f);
	gluSphere(quadric, 0.401f, 100, 100);

}


void KundaliRahu()
{
	//Rahu
	glTranslatef(7.5f, -2.5f, -20.0f);
	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(angle, 0.0f, -1.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 0.0f, 0.0f);
	gluPartialDisk(quadric, 0.30, 0.40, 100, 1, 30, 300);
	
	glLineWidth(10);
	glBegin(GL_LINES);

	glVertex3f(0.17f, 0.35f, 0.0f); 
	glVertex3f(0.55f, 0.35, 0.0f); 

	glVertex3f(-0.17f, 0.35f, 0.0f);
	glVertex3f(-0.55f, 0.35, 0.0f);

	glEnd();
}

void KundaliKetu()
{
	//Ketu
	glTranslatef(-8.0f, 2.0f, -20.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.0f, 1.0f, 0.0f);
	glRotatef(angle, 0.0f, -1.0f, 0.0f);
	gluPartialDisk(quadric, 0.30, 0.40, 100, 1, 30, 300);

	glLineWidth(10);
	glBegin(GL_LINES);

	glVertex3f(0.17f, 0.35f, 0.0f);
	glVertex3f(0.55f, 0.35, 0.0f);

	glVertex3f(-0.17f, 0.35f, 0.0f);
	glVertex3f(-0.55f, 0.35, 0.0f);

	glEnd();
}

void KundaliAnimation(void)
{	
	// Rotate planets at the end 
	angle = angle + 1.0f;
	if (angle >= 360.0f)
		angle = 0.0f;

	if (Saturnz >= -15.0f)
	{
		Saturnz = Saturnz - KundaliPlanetSpeed;
		if (Saturnx < 3.3f)
			Saturnx = Saturnx + KundaliXspeed;
		if (Saturny > -3.1f)
			Saturny = Saturny - KundaliYspeed;
		if (Saturnz <= -15.0f)
		{
			Saturnx = 3.3f;
			Saturny = -3.1f;
			Venusz = 0.0f;
		}


	}
	else if (Venusz >= -15.0f)
	{
		Venusz = Venusz - KundaliPlanetSpeed;
		if (Venusx < 0.8f)
			Venusx = Venusx + KundaliXspeed;
		if (Venusy < 2.7f)
			Venusy = Venusy + KundaliYspeed;
		if (Venusz <= -15.0f)
		{
			Venusx = 0.8f;
			Venusy = 2.7f;
			Jupiterz = 0.0f;
		}
	}
	else if (Jupiterz >= -15.0f)
	{
		Jupiterz = Jupiterz - KundaliPlanetSpeed;
		if (Jupiterx > -0.8f)
			Jupiterx = Jupiterx - KundaliXspeed;
		if (Jupitery < 2.7f)
			Jupitery = Jupitery + KundaliYspeed;
		if (Jupiterz <= -15.0f)
		{
			Jupiterx = -0.8f;
			Jupitery = 2.7f;
			Plutoz = 0.0f;
		}
	}
	else if (Plutoz >= -15.0f)
	{
		Plutoz = Plutoz - KundaliPlanetSpeed;
		if (Plutoy < 2.0f)
			Plutoy = Plutoy + KundaliYspeed;
		if (Plutoz <= -15.0f)
		{
			Plutox = 0.0f;
			Plutoy = 2.0f;
			Sunz = 0.0f;
		}
	}
	else if (Sunz >= -15.0f)
	{
		Sunz = Sunz - KundaliPlanetSpeed;
		if (Sunx > -4.2f)
			Sunx = Sunx - KundaliXspeed;
		if (Suny < 3.5f)
			Suny = Suny + KundaliYspeed;
		if (Sunz <= -15.0f)
		{
			Sunx = -4.2f;
			Suny = 3.5f;
			Uranusz = 0.0f;
		}
	}
	else if (Uranusz >= -15.0f)
	{
		Uranusz = Uranusz - KundaliPlanetSpeed;
		if (Uranusx > -3.0f)
			Uranusx = Uranusx - KundaliXspeed;
		if (Uranusy < 3.5f)
			Uranusy = Uranusy + KundaliYspeed;
		if (Uranusz <= -15.0f)
		{
			Uranusx = -3.0f;
			Uranusy = 3.5f;
			Marsz = 0.0f;
		}
	}
	else if (Marsz >= -15.0f)
	{
		Marsz = Marsz - KundaliPlanetSpeed;
		if (Marsx > -3.2f)
			Marsx = Marsx - KundaliXspeed;
		if (Marsz <= -15.0f)
		{
			Marsx = -3.2f;
			Marsy = 0.0f;
			Mercuryz = 0.0f;
		}
	}
	else if (Mercuryz >= -15.0f)
	{
		Mercuryz = Mercuryz - KundaliPlanetSpeed;
		if (Mercuryx > -6.1f)
			Mercuryx = Mercuryx - KundaliXspeed - 0.0045;
		if (Mercuryy < 3.1f)
			Mercuryy = Mercuryy + KundaliYspeed;
		if (Mercuryz <= -15.0f)
		{
			Mercuryx = -6.1f;
			Mercuryy = 3.1f;
			Neptunez = 0.0f;
		}
	}
	else if (Neptunez >= -15.0f)
	{
		Neptunez = Neptunez - KundaliPlanetSpeed;
		if (Neptunex > -5.0f)
			Neptunex = Neptunex - KundaliXspeed - 0.0045;
		if (Neptuney < 2.4f)
			Neptuney = Neptuney + KundaliYspeed;
		if (Neptunez <= -15.0f)
		{
			Neptunex = -5.0f;
			Neptuney = 2.4f;
			Moonz = 0.0f;
		}
	}

	else if (Moonz >= -15.0f)
	{
		Moonz = Moonz - KundaliPlanetSpeed;
		if (Moonx > -3.2f)
			Moonx = Moonx - KundaliXspeed - 0.0003;
		if (Moony > -3.1f)
			Moony = Moony - KundaliYspeed - 0.0003;
		if (Moonz <= -15.0f)
		{
			Moonx = -3.2f;
			Moony = -3.1f;
			drawKundaliLines = 1;
		}
	}



	else if (drawKundaliLines == 1)
	{
		if (KundaliLinesColorg < 0.3f) // 0.3  is G color of kundali lines
		{
			KundaliLinesColorg = KundaliLinesColorg + 0.0001;
			KundaliLinesColorb = KundaliLinesColorb + 0.0002;
		}

		if (KundaliLinesColorg >= 0.3f)
		{
			LetterAstromedicomp = 1;

			Scene3timer = Scene3timer + 1.0;

			if (Scene3timer >= 400)
			{
				Scene_SolarSystemAnimation = false; // flag for next scene start
													//glClearColor(0.0f, 1.0f, 0.0f, 0.0f); // use this to go to next animation 
			}

		}
	}

}