#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include"ThankYouNote.h"
#define TIMER_LAST_THANK_YOU_NOTE 133

GLuint Texture_last_note;
bool drawLastThankYouNote = true;
extern HWND gHwnd;
bool Scene_LastThankYouNote = false;
void scene_lastNoteInitialize() {
	LoadGLTextures(&Texture_last_note, MAKEINTRESOURCE(IDBITMAP_LAST_NOTE));

	SetTimer(gHwnd, TIMER_LAST_THANK_YOU_NOTE, 10000, (TIMERPROC)turnOfThankYouNoteDrawingCallback);
}
void drawLastNoteTexture(void) {

	//glScalef(1.0, 1.0, 1.0);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, Texture_last_note);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);


	//front
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.7f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.7f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.7f, -1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.7f, -1.0f, 0.0f);

	glEnd();
	glDisable(GL_TEXTURE_2D);
}




void scene_lastNoteDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (drawLastThankYouNote) {
		glTranslatef(0.0, 0.0f, -2.5f);
		drawLastNoteTexture();
	}
}


void turnOfThankYouNoteDrawingCallback() {
	drawLastThankYouNote = false;
	KillTimer(gHwnd, TIMER_LAST_THANK_YOU_NOTE);
	Scene_LastThankYouNote = true;
}

