#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>

#include "DrawChars.h"
#include "CertificateNames.h"
#define CERTIFICATES_SCENE_WAIT_TIMER 55
using namespace std;

//global variable declarations
//for drawing characters
DrawChars *drawCharCertNames = NULL;

//for animation of characters
GLfloat xTranslationOfALineCertNames = 85.0f;
bool CertificateNameTranslationCompleteFlag = false;

GLint noOfLinesToDrawCertNames = 5;


//for displaying logos
const float PI = 3.1415f;
float angleCertNames;
GLint CirclePoints = 10000;
extern HWND gHwnd;
typedef enum
{
    RED = 0,
    GREEN,
    BLUE,
    YELLOW,
} color_t;


void setTextToDisplayCertNames(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw)
{

	
	drawCharCertNames->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
		traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

}



void scene_certificateNameDisplay(void)
{
	//function prototypes
	void drawCertificateNames(void);
	void DrawMicrosoftLogo();
	void DrawAppleLogo();

	vector<GLfloat> traslateVec;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	vector<string>  textToDraw;
	textToDraw.resize(6);
	textToDraw[0] = "Certifications";
	textToDraw[1] = "Final Cut Studio 2 Master Pro";
	textToDraw[2] = "Final Cut Studio Master Pro";
	textToDraw[3] = "Heterogeneous Parallel Programming";
	textToDraw[4] = "Adobe Certified Expert in Premiere Pro CS 6";
	textToDraw[5] = "ARM Accredited Engineer";

	vector<string>  textToDisplayString;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayString.push_back(textToDraw[i]);


	vector<double> wordSpacVec;
	wordSpacVec.resize(6);
	wordSpacVec[0] = 2.0;
	wordSpacVec[1] = 2.0;
	wordSpacVec[2] = 2.0;
	wordSpacVec[3] = 2.0;
	wordSpacVec[4] = 2.0;
	wordSpacVec[5] = 2.0;

	vector<double>  textToDisplayWordSpac;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayWordSpac.push_back(wordSpacVec[i]);



	vector<double> charSpacVec;
	charSpacVec.resize(6);

	charSpacVec[0] = 3.2;
	charSpacVec[1] = 1.9;
	charSpacVec[2] = 1.9;
	charSpacVec[3] = 1.9;
	charSpacVec[4] = 1.9;
	charSpacVec[5] = 1.9;

	vector<double>  textToDisplayCharSpac;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayCharSpac.push_back(charSpacVec[i]);


	vector<double> charWdth;
	charWdth.resize(6);

	charWdth[0] = 1.5;
	charWdth[1] = 1.5;
	charWdth[2] = 1.5;
	charWdth[3] = 1.5;
	charWdth[4] = 1.5;
	charWdth[5] = 1.5;


	vector<double>  textToDisplayCharWdth;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayCharWdth.push_back(charWdth[i]);



	vector<double> traslateXParam;
	traslateXParam.resize(6);

	traslateXParam[0] = xTranslationOfALineCertNames;
	traslateXParam[1] = xTranslationOfALineCertNames;
	traslateXParam[2] = xTranslationOfALineCertNames;
	traslateXParam[3] = xTranslationOfALineCertNames;
	traslateXParam[4] = xTranslationOfALineCertNames;
	traslateXParam[5] = xTranslationOfALineCertNames;

	vector<double>  textToDisplayTranslateXParam;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayTranslateXParam.push_back(traslateXParam[i]);
	
		


	vector<double> traslateYParam;
	traslateYParam.resize(6);

	traslateYParam[0] = 12.0;
	traslateYParam[1] = 0.0;
	traslateYParam[2] = -6.0;
	traslateYParam[3] = -12.0;
	traslateYParam[4] = -18.0;
	traslateYParam[5] = -24.0;

	vector<double>  textToDisplayTranslateYParam;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayTranslateYParam.push_back(traslateYParam[i]);


	vector<double> traslateZParam;
	traslateZParam.resize(6);

	traslateZParam[0] = -98.0;
	traslateZParam[1] = -98.0;
	traslateZParam[2] = -98.0;
	traslateZParam[3] = -98.0;
	traslateZParam[4] = -98.0;
	traslateZParam[5] = -98.0;

	vector<double>  textToDisplayTranslateZParam;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayTranslateZParam.push_back(traslateZParam[i]);


	vector<double> rgbVec1{ 0.0, 1.0, 1.0 };
	vector<double> rgbVec2{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec3{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec4{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec5{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec6{ 1.0, 1.0, 1.0 };

	vector<vector<double>>rgbValueVec;
	rgbValueVec.resize(6);
	rgbValueVec[0] = rgbVec1;
	rgbValueVec[1] = rgbVec2;
	rgbValueVec[2] = rgbVec3;
	rgbValueVec[3] = rgbVec4;
	rgbValueVec[4] = rgbVec5;
	rgbValueVec[5] = rgbVec6;

	vector<vector<double>>  textToDisplayRGBValueVec;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayRGBValueVec.push_back(rgbValueVec[i]);


	vector<double> scaleVec1{ 0.6f, 0.6f, 0.0f };
	vector<double> scaleVec2{ 0.3f, 0.3f, 0.0f };
	vector<double> scaleVec3{ 0.3f, 0.3f, 0.0f };
	vector<double> scaleVec4{ 0.3f, 0.3f, 0.0f };
	vector<double> scaleVec5{ 0.3f, 0.3f, 0.0f };
	vector<double> scaleVec6{ 0.3f, 0.3f, 0.0f };


	vector<vector<double>>scaleValueVec;
	scaleValueVec.resize(6);
	scaleValueVec[0] = scaleVec1;
	scaleValueVec[1] = scaleVec2;
	scaleValueVec[2] = scaleVec3;
	scaleValueVec[3] = scaleVec4;
	scaleValueVec[4] = scaleVec5;
	scaleValueVec[5] = scaleVec6;

	vector<vector<double>>  textToDisplayScaleValueVec;
	for (int i = 0; i <= noOfLinesToDrawCertNames; i++)
		textToDisplayScaleValueVec.push_back(scaleValueVec[i]);

	setTextToDisplayCertNames(textToDisplayWordSpac, textToDisplayCharSpac, textToDisplayCharWdth, textToDisplayTranslateXParam, textToDisplayTranslateYParam, textToDisplayTranslateZParam, textToDisplayRGBValueVec, textToDisplayScaleValueVec, textToDisplayString);

	drawCharCertNames->setWordToDisplayOnScreen();

	glPopMatrix();

	glLoadIdentity();
	glTranslatef((xTranslationOfALineCertNames -25.0f), 1.5f, -98.0f);
	DrawAppleLogo();
	

	glLoadIdentity();
	glTranslatef((xTranslationOfALineCertNames - 25.0f) / 5.0f, -2.5f, -15.0f);
	DrawMicrosoftLogo();
    void updateCertNames(void);
    updateCertNames();
}
void MarkCertificateSceneDoneCallback() {
	CertificateNameTranslationCompleteFlag = true;
	KillTimer(gHwnd, CERTIFICATES_SCENE_WAIT_TIMER);
}
void updateCertNames(void)
{
	if (xTranslationOfALineCertNames >= -28.0f)
		xTranslationOfALineCertNames = xTranslationOfALineCertNames - 0.05f;

}

void DrawMicrosoftLogo()
{


	// function prototype
	void DrawCircle(void);
	void DrawLeftRectangle(GLfloat, GLfloat, color_t);
	void DrawRightRectangle(GLfloat, GLfloat, color_t);

	// Variable declaration
	GLfloat OrangeRectangleX = -0.25f;
	GLfloat OrangeRectangleY = -0.28f;
	GLfloat GreenRectangleX = 0.43f;
	GLfloat GreenRectangleY = 1.00f;
	GLfloat BlueRectangleX = -0.37f;
	GLfloat BlueRectangleY = -0.95f;
	GLfloat YellowRectangleX = 0.31f;
	GLfloat YellowRectangleY = 0.33f;

	glPushMatrix();
	DrawCircle();
	glPopMatrix();

	glPushMatrix();
	DrawLeftRectangle(OrangeRectangleX, OrangeRectangleY, RED);
	glPopMatrix();

	glPushMatrix();
	DrawRightRectangle(GreenRectangleX, GreenRectangleY, GREEN);
	glPopMatrix();

	glPushMatrix();
	DrawLeftRectangle(BlueRectangleX, BlueRectangleY, BLUE);
	glPopMatrix();

	glPushMatrix();
	DrawRightRectangle(YellowRectangleX, YellowRectangleY, YELLOW);
	glPopMatrix();

}

void DrawLeftRectangle(GLfloat x, GLfloat y, color_t Color)
{
	// variable declarations
	GLfloat j = -0.01f;

	// Code
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(x, y, -5.0f);
	glBegin(GL_LINES);
	for (int i = 2000; i < 2900; i++)
	{
		angleCertNames= 2 * PI * i / CirclePoints;
		if (Color == RED)
		{
			glColor3f(0.96f + j, 0.32f + j, 0.078f);// 246, 83, 20
		}
		else if (Color == BLUE)
		{
			glColor3f(0.0f, 0.63f + j, 0.94f + j); // 0, 161, 241
		}
		j = j - 0.001f;
		glVertex3f(cos(angleCertNames), sin(angleCertNames), 0.0f);
		glVertex3f(cos(angleCertNames) - 0.1f, sin(angleCertNames) - 0.6f, 0.0f);
	}
	glEnd();
}

void DrawRightRectangle(GLfloat x, GLfloat y, color_t Color)
{
	// variable declarations
	GLfloat j = -0.01f;

	// Code
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(x, y, -5.0f);
	glBegin(GL_LINES);
	for (int i = 7000; i < 7900; i++)
	{
		angleCertNames= 2 * PI * i / CirclePoints;
		if (Color == GREEN)
		{
			glColor3f(0.49f + j, 0.73f + j, 0.0f); //  124, 187, 0
		}
		else if (Color == YELLOW)
		{
			glColor3f(1.0f + j, 0.73f + j, 0.0f); // 255, 187, 0
		}
		j = j - 0.001f;
		glVertex3f(cos(angleCertNames), sin(angleCertNames) + 0.6f, 0.0f);
		glVertex3f(cos(angleCertNames) - 0.1f, sin(angleCertNames), 0.0f);
	}
	glEnd();
}

void DrawCircle(void)
{
	// variable declarations
	GLfloat j = -0.001f;

	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0.0f, 0.0f, -5.0f);
	glBegin(GL_POLYGON);
	for (int i = 0; i < CirclePoints; i++)
	{
		angleCertNames= 2 * PI * i / CirclePoints;
		glColor3f(0.0f, 0.4f + j, 0.8f + j);
		glVertex3f(cos(angleCertNames), sin(angleCertNames), 0.0f);
		j = j - 0.0001f;
	}
	glEnd();
}

//Apple Logo
void DrawAppleLogo()
{
	// function prototype
	//void DrawCircleUsingLines(GLfloat);
	//void DrawHorizontalLine(GLfloat);
	void DrawCircleUsingLines(GLfloat, GLfloat, GLfloat, int);
	void DrawRectangle(GLfloat, GLfloat);

	// variable declaration
	GLfloat radius = 6.5f;
	//GLfloat outerRadius = 0.7f;
	GLfloat centerXAxis = 0.0f;
	GLfloat centerYAxis = 0.0f;

	DrawCircleUsingLines(radius, centerXAxis, centerYAxis, 1); // center circle diameter = 13
															   //DrawRectangle(radius, radius); // rectangle around center
															   //DrawCircleUsingLines(4, x16, y16, 16); // diameter 8
	DrawCircleUsingLines(4, -0.05f, 10.19f, 16); // diameter 8
	DrawCircleUsingLines(4.0, 3.50f, 7.0f, 3); // diameter 8
											   //DrawRectangle(radius, radius + 4.0f);
											   //DrawRectangle(radius + 1.5f, radius);
	DrawCircleUsingLines(4.0, -1.0f, 11.0f, 2); // diameter 8
												//DrawCircleUsingLines(4.0, x12, y12, 12); // diameter 8
	DrawCircleUsingLines(4.0, 3.30f, 2.95f, 12); // diameter 8
												 //DrawCircleUsingLines(6, x13, y13, 13); // diameter 12
	DrawCircleUsingLines(6, 1.65f, -1.15f, 13); // diameter 12
												//DrawCircleUsingLines(6, x14, y14, 14); // diameter 12
	DrawCircleUsingLines(6, -1.60f, -1.0f, 14); // diameter 12
												//DrawCircleUsingLines(6, x15, y15, 15); // diameter 12
	DrawCircleUsingLines(6, -2.40f, 0.45f, 15); // diameter 12
	DrawCircleUsingLines(4.0, 9.1f, 1.25f, 4); // diameter 8
	DrawCircleUsingLines(2.5, -3.0f, -4.5f, 7); // diameter 5
	DrawCircleUsingLines(0.5, 0.0f, -5.0f, 10); // diameter 1
	DrawCircleUsingLines(2.5, 3.0f, -4.5f, 8); // diameter 5
	DrawCircleUsingLines(1.0, 5.6f, -2.3f, 11); // diameter 2
												//DrawCircleUsingLines(4.0, 0.0f, -10.20f, 5); // diameter 8
												//DrawCircleUsingLines(4.0, x17, y17, 5); // diameter 8
	DrawCircleUsingLines(4.0, 0.0f, -10.40f, 5); // diameter 8
	DrawCircleUsingLines(4.0, -3.3f, 2.9f, 6); // diameter 8
	DrawCircleUsingLines(1.5, -6.1f, -1.8f, 9); // diameter 3
}

void DrawRectangle(GLfloat xAxis, GLfloat yAxis)
{
	xAxis = xAxis + 0.45f;
	yAxis = yAxis + 0.45f;
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-xAxis, yAxis, 0.0f);
	glVertex3f(-xAxis, -yAxis, 0.0f);
	glVertex3f(-xAxis, -yAxis, 0.0f);
	glVertex3f(xAxis, -yAxis, 0.0f);
	glVertex3f(xAxis, -yAxis, 0.0f);
	glVertex3f(xAxis, yAxis, 0.0f);
	glVertex3f(xAxis, yAxis, 0.0f);
	glVertex3f(-xAxis, yAxis, 0.0f);
	glEnd();
}

void DrawCircleUsingLines(GLfloat radius, GLfloat centerXAxis, GLfloat centerYAxis, int number)
{
	// Variable declarations
	const float PI = 3.1415f;
	GLint CirclePoints = 1000;
	float angle;

	glBegin(GL_LINES);
	if ((number == 1) || (number == 6) || (number == 7) || (number == 8) || (number == 9) || (number == 10) || (number == 11) || (number == 5) || (number == 4) || (number == 12) || (number == 13) || (number == 14) || (number == 15) || (number == 16) || (number == 2) || (number == 3))
	{
		glBegin(GL_POLYGON);
	}
	if ((number == 5) || (number == 4) || (number == 16))
	{
		glColor3f(0.0f, 0.0f, 0.0f);
	}
	else
	{
		glColor3f(1.0f, 1.0f, 1.0f);
	}
	if (number == 2)
	{
		for (int i = 770; i < 1000; i++)
		{
			angleCertNames= 2 * PI * i / CirclePoints;
			glVertex3f(radius * cos(angleCertNames) + centerXAxis, radius * sin(angleCertNames) + centerYAxis, 0.0f);
		}
	}
	else if (number == 3)
	{
		for (int i = 265; i < 500; i++)
		{
			angleCertNames= 2 * PI * i / CirclePoints;
			glVertex3f(radius * cos(angleCertNames) + centerXAxis, radius * sin(angleCertNames) + centerYAxis, 0.0f);
		}
	}
	else
	{
		for (int i = 0; i < CirclePoints; i++)
		{
			angleCertNames= 2 * PI * i / CirclePoints;
			glVertex3f(radius * cos(angleCertNames) + centerXAxis, radius * sin(angleCertNames) + centerYAxis, 0.0f);
		}
	}
	glEnd();
}

void scene_certificateNameInitialize(void)
{
	drawCharCertNames = new DrawChars();
	SetTimer(gHwnd, CERTIFICATES_SCENE_WAIT_TIMER, 20000, (TIMERPROC)MarkCertificateSceneDoneCallback);
}

