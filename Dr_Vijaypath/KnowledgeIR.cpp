
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>


#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
#include"KnowledgeIR.h"
#include"DrawChars.h"
#include"Resources.h"
#define KNOWLEDGE_IT_TIMER 505
using namespace std;
int currentNoOfCharsInKnowledge, currentNoOfCharsInIs, currentNoOfCharsInInterrelated;
std::string knowledgeText = "Knowledge";
std::string isText = "is";
std::string interrelatedText = "Inter-related";

GLuint Texture_Sir;
extern DrawChars *drawCharKIR = NULL;
extern HWND gHwnd;
bool isKnowledgeTextDone, isIsTextDone, isInterrelatedTextDone;
bool scene_KnowledgeIRFlag=false;
int LoadGLTextures(GLuint *, TCHAR[], bool);
int LoadGLTexturesKnowledgeIR(GLuint *texture, TCHAR imageResourceId[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	glGenTextures(1, texture);
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		DeleteObject(hBitmap);
	}

	return(iStatus);
}
void knowledgeIR_initialize() {
	glDisable(GL_LIGHTING);
	LoadGLTexturesKnowledgeIR(&Texture_Sir, MAKEINTRESOURCE(IDBITMAP_GOKHALESIR_WHITE_BK));
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);//set background to white
	drawCharKIR = new DrawChars();
	SetTimer(gHwnd, KNOWLEDGE_IT_TIMER, 700, (TIMERPROC)knowledgeIRUpdate);
	//glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
}
void knowledgeIR_uninitialize() {
	delete drawCharKIR;
	KillTimer(gHwnd, KNOWLEDGE_IT_TIMER);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //reset background to black
}
void drawSirWhiteBkTexture(void) {

	glScalef(0.75, 1.0, 1.0);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, Texture_Sir);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);


	//front
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(4.5f, 5.0f, 1.3f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-5.0f, 5.0f, 1.3f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-5.0f, -5.0f, 1.3f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(5.0f, -5.0f, 1.3f);

	glEnd();
	glDisable(GL_TEXTURE_2D);


}
void setTextToDisplayKnowledgeIR(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw)
{


	drawCharKIR->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
		traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

}

void knowledgeIRdisplay(void)
{

	glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0, 6.0f, -35.f);
	drawSirWhiteBkTexture();

	std::string currentChars;
	vector<string>  textToDraw;

	if (isKnowledgeTextDone == false) {
		for (int i = 0;i < currentNoOfCharsInKnowledge;i++) {
			currentChars.push_back(knowledgeText[i]);
		}
		textToDraw.push_back(currentChars);
	}
	else
		textToDraw.push_back(knowledgeText);

	//Before we draw next string, clear current chars
	currentChars.clear(); //reset for next

	if (isIsTextDone == false) {
		for (int i = 0;i < currentNoOfCharsInIs;i++) {
			currentChars.push_back(isText[i]);
		}
		textToDraw.push_back(currentChars);
	}
	else
		textToDraw.push_back(isText);

	//Before we draw next string, clear current chars
	currentChars.clear(); //reset for next

	if (isInterrelatedTextDone == false) {
		for (int i = 0;i < currentNoOfCharsInInterrelated;i++) {
			currentChars.push_back(interrelatedText[i]);
		}
		textToDraw.push_back(currentChars);
	}
	else
		textToDraw.push_back(interrelatedText);

	vector<double> wordSpacVec;
	wordSpacVec.resize(10);
	wordSpacVec[0] = 0.0f;
	wordSpacVec[1] = 0.0f;
	wordSpacVec[2] = 0.0f;


	vector<double> charSpacVec;
	charSpacVec.resize(10);

	charSpacVec[0] = 1.2f;
	charSpacVec[1] = 0.8f;
	charSpacVec[2] = 0.9f;


	vector<double> charWidth;
	charWidth.resize(10);

	charWidth[0] = 3.5;
	charWidth[1] = 3.5;
	charWidth[2] = 3.5;



	vector<double> traslateXParam;
	traslateXParam.resize(10);


	traslateXParam[0] = -7.0f;
	traslateXParam[1] = -3.1f;
	traslateXParam[2] = -7.7f;


	vector<double> traslateYParam;
	traslateYParam.resize(10);

	traslateYParam[0] = -1.0f;
	traslateYParam[1] = -3.5f;
	traslateYParam[2] = -5.5f;

	vector<double> traslateZParam;
	traslateZParam.resize(10);

	traslateZParam[0] = -23.0;
	traslateZParam[1] = -23.0;
	traslateZParam[2] = -23.0;


	vector<double> rgbVec1{ 1.0f, 0.6f, 0.2f };
	vector<double> rgbVec2{ 0.6f, 0.6f, 0.6f };
	//vector<double> rgbVec2{ 1.0, 1.0, 1.0,gAnatomyBlend};
	vector<double> rgbVec3{ 0.0f, 1.0f, 0.0f };


	vector<vector<double>>rgbValueVec;
	rgbValueVec.resize(10);

	rgbValueVec[0] = rgbVec1;
	rgbValueVec[1] = rgbVec2;
	rgbValueVec[2] = rgbVec3;



	vector<double> scaleVec1{ 0.2f, 0.2f, 1.0f };
	vector<double> scaleVec2{ 0.2f, 0.2f, 1.0f };
	vector<double> scaleVec3{ 0.2f, 0.2f, 1.0f };

	vector<vector<double>>scaleValueVec;
	scaleValueVec.resize(10);

	scaleValueVec[0] = scaleVec1;
	scaleValueVec[1] = scaleVec2;
	scaleValueVec[2] = scaleVec3;


	setTextToDisplayKnowledgeIR(wordSpacVec, charSpacVec, charWidth, traslateXParam, traslateYParam, traslateZParam, rgbValueVec, scaleValueVec, textToDraw);

	drawCharKIR->setWordToDisplayOnScreen();


	//SwapBuffers(ghdc);
}
DWORD WINAPI PlayKeyBoardStrokeSoundCallBack(_In_ LPVOID lpParameter) {
	//PlaySound(MAKEINTRESOURCE(IDR_IDB_KEYBOARD_SOUND_WAVE), GetModuleHandle(NULL), SND_RESOURCE);
	PlaySound(TEXT("KnowledgeIR\\keyboard_stroke_sound.wav"), NULL, SND_FILENAME |SND_ASYNC);
	return 0;
}

void knowledgeIRUpdate(void)
{
	if (currentNoOfCharsInKnowledge < knowledgeText.size()) {
		currentNoOfCharsInKnowledge += 1;
		PlaySound(TEXT("KnowledgeIR\\keyboard_stroke_sound.wav"), NULL, SND_FILENAME);
	}
	else
		isKnowledgeTextDone = true;

	if (isKnowledgeTextDone) {
		if (currentNoOfCharsInIs < isText.size()) {
			currentNoOfCharsInIs += 1;
			currentNoOfCharsInKnowledge += 1;
			PlaySound(TEXT("KnowledgeIR\\keyboard_stroke_sound.wav"), NULL, SND_FILENAME );
		}
		else
			isIsTextDone = true;
	}
	if (isIsTextDone) {
		if (currentNoOfCharsInInterrelated < interrelatedText.size()) {
			currentNoOfCharsInInterrelated += 1;
			currentNoOfCharsInKnowledge += 1;
			PlaySound(TEXT("KnowledgeIR\\keyboard_stroke_sound.wav"), NULL, SND_FILENAME );
		}
		else {
			isInterrelatedTextDone = true;
			scene_KnowledgeIRFlag = true;
		}
	}
}