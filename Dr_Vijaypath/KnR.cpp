
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>


#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
#include"KnR.h"
#include"DrawChars.h"

using namespace std;

GLUquadric *quadricDiskKnR = NULL;
GLfloat bigCAngle=0.0f, bigCAngleLimit=281.0f;
DrawChars *drawCharKnR = NULL;
bool Scene_KnRAnimationAnimation=true;
extern HDC ghdc = NULL;

void scene_KnRInitialize(void)
{
	glDisable(GL_LIGHTING);
	void setTextToDisplayKnR();
	drawCharKnR = new DrawChars(); 
	setTextToDisplayKnR();
}

void setTextToDisplayKnR()
{
    int numOfLines = 9;

    vector<string>  textToDraw;
    textToDraw.resize(numOfLines);
	textToDraw[0] = "Eastern";
	textToDraw[1] = "Economy";
	textToDraw[2] = "Edition";
    textToDraw[3] = "SECOND EDITION";
    textToDraw[4] = "THE";
    textToDraw[5] = "PROGRAMMING";
    textToDraw[6] = "LANGUAGE";
	textToDraw[7] = "BRIAN W. KERNIGHAN";
	textToDraw[8] = "DENNIS M. RITCHIE";



    vector<double> wordSpacVec(numOfLines);
    wordSpacVec[0] = 0.0;
    wordSpacVec[1] = 0.0;
    wordSpacVec[2] = 0.0;
    wordSpacVec[3] = 2.0; //second edition
	wordSpacVec[4] = 0.0;
	wordSpacVec[5] = 0.0;
	wordSpacVec[6] = 0.0;
	wordSpacVec[7] = 1.5;
	wordSpacVec[8] = 1.5;


    vector<double> charSpacVec(numOfLines);
  	charSpacVec[0] = 1.0;
	charSpacVec[1] = 1.0;
	charSpacVec[2] = 1.0;
	charSpacVec[3] = 1.5;//SE
	charSpacVec[4] = 3.5;
	charSpacVec[5] = 3.0;
	charSpacVec[6] = 3.0;
	charSpacVec[7] = 1.75;
	charSpacVec[8] = 1.75;

    vector<double> charWdth(numOfLines);
    charWdth[0] = 2.0;
    charWdth[1] = 2.0;
    charWdth[2] = 2.0;
    charWdth[3] = 2.0;//SE
	charWdth[4] = 2.5;
	charWdth[5] = 3.0;
	charWdth[6] = 3.0;
	charWdth[7] = 2.0;
	charWdth[8] = 2.0;

    vector<double> rgbVec1{ 0.0, 0.0, 0.0 };
    vector<double> rgbVec2{ 0.0, 0.0, 0.0 };
    vector<double> rgbVec3{ 0.0, 0.0, 0.0 };
    vector<double> rgbVec4{ 0.0, 0.0, 0.0 };

    vector<vector<double>>rgbValueVec(numOfLines);
    rgbValueVec[0] = rgbVec1;
    rgbValueVec[1] = rgbVec2;
    rgbValueVec[2] = rgbVec3;
    rgbValueVec[3] = rgbVec4;
	rgbValueVec[4] = rgbVec1;
	rgbValueVec[5] = rgbVec2;
	rgbValueVec[6] = rgbVec3;
	rgbValueVec[7] = rgbVec4;
	rgbValueVec[8] = rgbVec4;

    vector<double> traslateXParam(numOfLines); //this will be the initial pt on x axis -2 where we want to draw
    traslateXParam[0] =  19.0;
    traslateXParam[1] =  21.0;
    traslateXParam[2] =  22.0;
    traslateXParam[3] =  -15.0;//second edition
	traslateXParam[4] =  -10.0;
	traslateXParam[5] =  -20.0;
	traslateXParam[6] =  -20.0;
	traslateXParam[7] =  -15.0;
	traslateXParam[8] =  -15.0;

    vector<double> traslateYParam(numOfLines);
    traslateYParam[0] =  37.5;
    traslateYParam[1] =  36.0;
    traslateYParam[2] =  34.5;
    traslateYParam[3] =  25.5;//second edition
	traslateYParam[4] =  18.0; //THE
	traslateYParam[5] = -22.0;
	traslateYParam[6] = -29.0;
	traslateYParam[7] = -34.0;
	traslateYParam[8] = -38.0;


    vector<double> traslateZParam(numOfLines);
    traslateZParam[0] = -98.0;
    traslateZParam[1] = -98.0;
    traslateZParam[2] = -98.0;
    traslateZParam[3] = -98.0;
	traslateZParam[4] = -98.0;
	traslateZParam[5] = -98.0;
	traslateZParam[6] = -98.0;
	traslateZParam[7] = -98.0;
	traslateZParam[8] = -98.0;

	

	vector<double> scaleVec1{ 0.1f, 0.1f, 1.0f };//Easten Edition
	vector<double> scaleVec2{ 0.25f, 0.25f, 1.0f };//Second Edition
	vector<double> scaleVec3{ 0.5f, 0.5f, 1.0f };//THE
	vector<double> scaleVec4{ 0.5f, 0.5f, 1.0f };//Program Lang
	vector<double> scaleVec5{ 0.25f, 0.25f, 1.0f };//DENNIS M. RITCHIE



	vector<vector<double>>scaleValueVec(numOfLines);
	scaleValueVec[0] = scaleVec1;
	scaleValueVec[1] = scaleVec1;
	scaleValueVec[2] = scaleVec1;
	scaleValueVec[3] = scaleVec2;
	scaleValueVec[4] = scaleVec3;
	scaleValueVec[5] = scaleVec4;
	scaleValueVec[6] = scaleVec4;
	scaleValueVec[7] = scaleVec5;
	scaleValueVec[8] = scaleVec5;	
	
    
    drawCharKnR->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
                           traslateZParam, rgbValueVec, scaleValueVec, textToDraw);
	
}


void drawBigCKnR()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -98.0f);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    quadricDiskKnR = gluNewQuadric();
    // glRotatef(30.0f, 0.0f, 0.0f, 1.0f);
    glScalef(12.0f, 12.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 0.0f);
    gluPartialDisk(quadricDiskKnR, 0.5, 1.0, 100, 1, 120, bigCAngle);//inner 
    glPopMatrix();
}


void drawSingleHzLineKnR(vector<GLfloat> rgbVals, GLfloat wdth, GLfloat yValue)
{

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLineWidth(wdth);
    glTranslatef(0.0f, yValue, -98.0f);
    glBegin(GL_LINES);
    glColor3f(rgbVals[0], rgbVals[1], rgbVals[2]);
    glVertex3f(-20.0f, 15.0f, 0.0f);
    glVertex3f(20.0f, 15.0f, 0.0f);
    glEnd();

}


void drawHorizontalLinesKnR()
{
    GLfloat wd = 3.0f;
    GLfloat yValue = 2.0f;
    vector<GLfloat> rgbOrange, rgbBlack;
	rgbOrange.resize(3);
	rgbBlack.resize(3);
	rgbOrange[0] = 1.0; rgbOrange[1] = 0.5;  rgbOrange[2] = 0.0;
	rgbBlack[0]  = 0.0;  rgbBlack[1] = 0.0;   rgbBlack[2] = 0.0;
	drawSingleHzLineKnR(rgbOrange, 0.5, 10.5);//below edition thin orange line 
    drawSingleHzLineKnR(rgbOrange, 3.0, 10.0);//below edition thick orange line 
	drawSingleHzLineKnR(rgbBlack, 3.0, 0.0);//below "THE" thick black line 
	drawSingleHzLineKnR(rgbBlack, 2.0, -28.0);//below "C" thick black line 
	drawSingleHzLineKnR(rgbOrange, 3.0, -45.0);//below Progr lang thick orange line 
	drawSingleHzLineKnR(rgbOrange, 0.5, -44.5);
}



void drawRectangleKnR()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -98.0f);
    glBegin(GL_QUADS);
    glColor3ub(0, 204, 204);
    glVertex3f(-30.0f,  40.0f, 0.0f);
    glVertex3f(-30.0f, -40.0f, 0.0f);
    glVertex3f(30.0f, -40.0f, 0.0f);
    glVertex3f(30.0f, 40.0f, 0.0f);
    glEnd();
}


void scene_KnRDisplay(void)
{
	KnRUpdate();
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
	//glScalef(0.2f,0.2f,0.2f);
    drawRectangleKnR();
    drawHorizontalLinesKnR();
    drawBigCKnR();
	drawCharKnR->setWordToDisplayOnScreen();
    SwapBuffers(ghdc);
}

void uninitializeKnR(void)
{
	gluDeleteQuadric(quadricDiskKnR);

}
void KnRUpdate(){
		if(bigCAngle < bigCAngleLimit ){
			bigCAngle += 0.02f;
		}else
			Scene_KnRAnimationAnimation=false;
}