#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include<gl\GL.h>
#include<gl\glu.h>
#include"human_heart_c_impl_all.h"
#include"Resources.h"
#include"Medical.h"

//Libraries
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define MY_NAME_INITIAL "JCG"
#define HEART_PUMP_TIMER 1


bool scene_Medical = true;

//----ECG---specific
#define ID_TIMER 2
#define NO_OF_SECONDS 160
#define MAX_POINTS_LIMIT 25000
GLfloat gfCurrentCardioX = -1.0f, gfCurrentCardioY = 0.0f;
GLfloat gfEndCardioX = 0.45f, gfEndCardioY = 0.0f;
bool IsCardioLineDrawDone = false;
bool displayHeart = true; //Have not used yet
float randomFloat(float a, float b);
float xView, yView, zView = 1.8f;
float graphRotationAngle;
bool drawSpehere = false;
typedef struct point {
	GLfloat x, y;
}point_t;
typedef struct cardioLinePoint {
	point_t current;
	point_t previous;
	float distance;
}cardioLinePoint_t;
typedef struct cardioLine {
	cardioLinePoint position[NO_OF_SECONDS];
	GLint arrayLength;
	point_t tipPosition;
}cardioLine_t;

cardioLine_t cardioLine;
point_t linePoints[MAX_POINTS_LIMIT];
GLfloat pointsArray[MAX_POINTS_LIMIT][2];
int lineEndIndex = 0;
void generateRandomNumbersArray(int count);
void ECGDisplay();
DWORD WINAPI PlayHeartBeatSound(_In_ LPVOID lpParameter);
float randomFloat(float a, float b);
void move();
void resetCardioLine();




//GLfloat xView, yView, zView = 1.8f;
GLfloat yRotationAngle = 0.0f;
GLfloat heartEndZ = -90.0f;
GLfloat scaleX=1.0f, scaleY=1.0f, scaleZ=1.0f;
GLfloat pumpScale = 0.4f;
bool isScaledOut = true;

// Lights
bool bLight = true; //for whether lighting is ON/OFF, by default 'off'
GLfloat LightAmbient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat LightDiffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
GLuint	Texture_Skin;
int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[]);

//_Amol_
void Medical_Initialize()
{
    glEnable(GL_TEXTURE_2D);
    glLightfv(GL_LIGHT6, GL_AMBIENT, LightAmbient); //setup ambient light
    glLightfv(GL_LIGHT6, GL_DIFFUSE, LightDiffuse); //setup diffuse light
    glLightfv(GL_LIGHT6, GL_POSITION, LightPosition); //position the light
    glEnable(GL_TEXTURE_2D); //enable texture mapping
    glEnable(GL_LIGHT6); //enable above configured LIGHT6
    //glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    LoadGLTextures(&Texture_Skin, MAKEINTRESOURCE(IDB_BITMAP1_SKIN));
    glBindTexture(GL_TEXTURE_2D, Texture_Skin);

    cardioLine.position[0].current.y = 0.0f;
    cardioLine.position[0].current.x = -1.0f;
    cardioLine.position[0].previous.x = -1.0f;
    cardioLine.position[0].previous.y = 0.0f;
    cardioLine.position[0].distance = 0.0f;
    resetCardioLine();
	PlaySound(NULL, NULL, NULL);
}



void heartDisplay() {

	//glEnable(GL_CULL_FACE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	if (zView < heartEndZ) { //Once heart is set at its position, turn on ECG
		glTranslatef(1.5f, 0.0f, -1.5f);
		ECGDisplay();
	}
	
	glTranslatef(-2.5f, yView, zView);
		
		glRotatef(yRotationAngle, 0.0f, 1.0f, 0.0f);

        if (yRotationAngle >= 100.0f)
            scene_Medical = false;
		//glScalef(scaleX, scaleY, scaleZ);
		
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, human_heart_part_allPositions);
		glNormalPointer(GL_FLOAT, 0, human_heart_part_allNormals);
		
		glTexCoordPointer(2, GL_FLOAT, 0, human_heart_part_allTexels);

		glDrawArrays(GL_TRIANGLES, 0, human_heart_part_allVertices);
		
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//generate mipmapped texture (3 bytes, width, height & data from bmp)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}

void heartUpdate() {
	
	if (zView > heartEndZ) {
		zView = zView - 0.05f;
	}

	if (yRotationAngle < 360.0f) {
		yRotationAngle += 0.01f;
	}
	else
		yRotationAngle = 0.0f;
	
}


/*
void pumpHeart() {
	if (isScaledOut) {
	scaleX += 0.1f;
	scaleZ += 0.1f;
	isScaledOut = false;
	}
	else {
	scaleX -= 0.1f;
	scaleZ -= 0.1f;
	isScaledOut = true;
	}
}

*/
//ECG specific
GLfloat getDistance(point_t p1, point_t p2) {
	float t1 = (float)pow((p1.x - p2.x), 2);
	float t2 = (float)pow((p1.y - p2.y), 2);
	float t3 = t1 + t2;
	float dist = sqrt(t3);

	return dist;
}

void ECGDisplay() {
	move();//Update
	
	void resetCardioLine();
	void drawCircleUsingPoints(GLfloat radius);
	static bool eraseAll = true;
	GLUquadric *quadric = NULL;
	
	if (IsCardioLineDrawDone == false) {
	
		glColor3ub(51, 255, 51);

		glEnableClientState(GL_VERTEX_ARRAY);
		glPointSize(5.0f);
		glVertexPointer(2, GL_FLOAT, 0, pointsArray);
		glDrawArrays(GL_POINTS, 0, lineEndIndex);
		glDisableClientState(GL_VERTEX_ARRAY);

		pointsArray[lineEndIndex][0] = cardioLine.tipPosition.x;
		pointsArray[lineEndIndex][1] = cardioLine.tipPosition.y;
		if (gfCurrentCardioX < gfEndCardioX)
			lineEndIndex = (++lineEndIndex) % MAX_POINTS_LIMIT;
		else {
			lineEndIndex = 0;
		}
	}
	else {
		//reset start coordinates
		resetCardioLine();
		eraseAll = true;
	}
	glColor3ub(255, 255, 255);
	//SwapBuffers(ghdc);
}
void generateRandomNumbersArray(int count) {
	GLfloat randNum;
	GLfloat distance = 0.0;
	GLint index = 0;
	float randStart = randomFloat(0.35f, 0.5f);
	point_t start = { -1.0f,randStart }, end = { -1.0f,0.0f };
	GLfloat x = -0.9f;
	bool flag = false;
	for (index = 1;index < count - 1;index++) {
		cardioLine.position[index].previous.x = cardioLine.position[index - 1].current.x;
		cardioLine.position[index].previous.y = cardioLine.position[index - 1].current.y;

		start.x = cardioLine.position[index].previous.x;
		start.y = cardioLine.position[index].previous.y;
		x = x + 0.05f;
		end.x = x;

		if (index % 2 == 0) {
			randNum = randomFloat(0.3f, 0.5f);// generate a random Y position
			flag = true;
		}
		else
			randNum = 0.0f;

		if (index % 3 == 0) {
			end.y = randNum;
			distance = getDistance(start, end);
			cardioLine.position[index].distance = distance;
			cardioLine.position[index].current.y = end.y;
			cardioLine.position[index].current.x = end.x;

			cardioLine.position[index + 1].previous.y = end.y;
			cardioLine.position[index + 1].previous.x = end.x;
			if (flag) {
				randNum = randomFloat(0.0f, -0.25f);
				flag = false;
			}
			else
				randNum = 0.0f;
			start.x = end.x;
			start.y = end.y;
			x = x + 0.05f;
			end.x = x;
			end.y = randNum;
			distance = getDistance(start, end);
			cardioLine.position[index + 1].distance = distance;
			cardioLine.position[index + 1].current.y = end.y;
			cardioLine.position[index + 1].current.x = end.x;
			index++;
			continue;
		}
		else {

			end.y = 0.0f;
		}
		distance = getDistance(start, end);
		cardioLine.position[index].distance = distance;
		cardioLine.position[index].current.y = end.y;
		cardioLine.position[index].current.x = end.x;

	}

}


DWORD WINAPI PlayHeartBeatSound(_In_ LPVOID lpParameter) {
	//PlaySound(TEXT("heart_bit_sound.wav"), NULL, SND_FILENAME);
	PlaySound(MAKEINTRESOURCE(IDR_WAVE2), GetModuleHandle(NULL), SND_RESOURCE);
	return 0;
}


bool getPointOnLine(point_t start, point_t end, GLfloat at, float distance, point_t* pointOnLine) {


	float t = at / distance;
	float x = (1 - t)*start.x + t*end.x;
	float y = (1 - t)*start.y + t*end.y;
	if (pointOnLine) {
		pointOnLine->x = x;
		pointOnLine->y = y;
	}
	else
		return false;
	return true;
}
void resetCardioLine() {
	cardioLine.tipPosition.x = -1.0f;
	cardioLine.tipPosition.y = 0.0f;
	gfCurrentCardioX = -1.0f;
	gfCurrentCardioY = 0.0f;
	IsCardioLineDrawDone = false;
	generateRandomNumbersArray(NO_OF_SECONDS);
}
void move() {
	static int index = 0;
	static GLfloat d2 = 0.0f,oldEndY;
	static point_t start, end;
	point_t pointOnLine;
	static float distanceBetCurrPoints = 0.0f;

	if (gfCurrentCardioX < gfEndCardioX) {
		gfCurrentCardioX = gfCurrentCardioX + 0.0001f;
	}
	else {
		IsCardioLineDrawDone = true;
		index = 0;
		lineEndIndex = 0;
		return;
	}

	if (d2 < distanceBetCurrPoints) {
		//calculate new Y directing towards currentY
		d2 = d2 + 0.0005;
		getPointOnLine(start, end, d2, distanceBetCurrPoints, &pointOnLine);
		cardioLine.tipPosition.y = pointOnLine.y;
		cardioLine.tipPosition.x = pointOnLine.x;
	}
	else {
		//pick new Y from array
		
		//Play heart beat sound asynchronously for peak value
		if (end.y > 0.25) {
			GLfloat scaleMargin = oldEndY - end.y;
			//pumpHeart(scaleMargin);
			CreateThread(
				NULL,                   // default security attributes
				0,                      // use default stack size  
				PlayHeartBeatSound,       // thread function name
				NULL,          // argument to thread function 
				0,                      // use default creation flags 
				NULL);
			
		}
		oldEndY = end.y;
		end.y = cardioLine.position[index].current.y;
		end.x = cardioLine.position[index].current.x;
		start.x = cardioLine.position[index].previous.x;
		start.y = cardioLine.position[index].previous.y;
		distanceBetCurrPoints = cardioLine.position[index].distance;
		index++;
		d2 = 0.0f;

	}

}
float randomFloat(float a, float b) {
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}
