//Header files.

#include <windows.h>
#include <math.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdlib.h>
#include <vector>
#include <time.h> 
#include"Resources.h"
#include"DrawChars.h"

#define SCENE_TEACHING_TIMER 233
//#include "sir_outline_coordinates.h"
bool scene_Teaching_partDone;
int first_imageCounter = 0;
float speed = 0.0f;
extern int coord_first[55434];
DrawChars *drawCharTeaching = NULL;
/*Texture variables for Sir's Image and BoardEdge*/

GLuint Wooden_Frame;
GLfloat gTranslateTexture = 1.0f;
bool isGurukulDone = false;
bool isOutlineDone = false;
bool doTranslateImage = false;
bool isDrawBoardDone = false;

/*SSB global Variables */
GLfloat xTrans = 10.0f;
GLint noOfLinesToDisplay = 10;
extern HWND gHwnd;

/*GluLookAt Parameter*/
//GLfloat xView, cX, yView, cY, zView = -4.0f, cZ = 0.3f;
GLfloat cX = 0.0, cY = 0.0, cZ = 0.0;

/*Variable for alphabets printing on Board*/
GLfloat gGurukul = -16.0f;
GLfloat gMedi_X = -80.0f;
GLfloat gAnatomy_X = -150.0f;
GLfloat gPhysiology_X = -180.0f;
GLfloat gComputer_X = -210.0f;
GLfloat gUnix_X = -240.0f;
GLfloat gSDK_X = -270.0f;
GLfloat gSeminar_X = -300.0f;
GLfloat gFundamental_X = -330.0f;
GLfloat gOS_X = -360.0f;
GLfloat gOpenGL_X = -390.f;

/*Variables for Color Blend */
GLfloat gGurukulBlendGreen = 0.392f;
GLfloat gGurukulBlendRed = 0.0f;

/* Flags */
GLint flag = 0;
GLfloat HideGreen = 1.0f;

/*Sir Image Hide variable*/
GLfloat gImageHideCube1 = 1.0f;
GLfloat gImageHideCube2 = 1.0f;

//new 
vector<int> randomPoints;
int sizeRandomPoints = 6000;
int CordSize = 0;


void UpdateSirOutline();
void DisplaySirOutline()
{
    if (isOutlineDone == false)
    {
        glTranslatef(0.0f, 0.0f, -2.0f);
    }
		
	else
		glTranslatef(3.0f, 0.0f, -6.5f);
	
    glPointSize(1.0f);
    glBegin(GL_POINTS);
    for (int i = 0; i < first_imageCounter; i += 2)
    {
        GLfloat x = (GLfloat)coord_first[i] / 1000;
        GLfloat y = (GLfloat)coord_first[i + 1] / 1000;
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(x, y);
    }
    glEnd();

    if (first_imageCounter  < CordSize - 2)
    {
        randomPoints.push_back(coord_first[first_imageCounter - 2] + (rand() % 5000 - 2500));
        randomPoints.push_back(coord_first[first_imageCounter - 1] + (rand() % 3000 - 1500));
    }

    if (randomPoints.size() > 2)
    {
        randomPoints.erase(randomPoints.begin());
        randomPoints.erase(randomPoints.begin());
    }
    glPointSize(2.0f);
    glBegin(GL_POINTS);
    for (int i = 0; i < (randomPoints.size() - 2); i += 2)
    {
        if (randomPoints.size() > 150)
        {
            int index;
            if (first_imageCounter + i < CordSize)
                index = first_imageCounter + i;
            else
                index = first_imageCounter - 4;

            GLfloat xDiff = (GLfloat)randomPoints[i] - coord_first[index];
            GLfloat x = (GLfloat)randomPoints[i] - (xDiff * ((float)(randomPoints.size() - i) / randomPoints.size()));
            x = x / 1000;

            GLfloat yDiff = (GLfloat)randomPoints[i + 1] - coord_first[index + 1];
            GLfloat y = (GLfloat)randomPoints[i + 1] - (yDiff * ((float)(randomPoints.size() - i) / randomPoints.size()));
            y = y / 1000;
            glColor3f(0.0f, 1.0f, 0.0f);
            glVertex2f(x, y);
        }
        else
        {
            isOutlineDone = true;
        }
    }
    glEnd();

	if (isOutlineDone == false)
		UpdateSirOutline();
	
}
void setTextToDisplayTeaching(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw)
{


	drawCharTeaching->initDrawChar(wordSpacVec, charSpacVec, charWdth, traslateXParam, traslateYParam,
		traslateZParam, rgbValueVec, scaleValueVec, textToDraw);
}
void MarkTeachingSceneDoneCallback() {
	scene_Teaching_partDone = true;
	KillTimer(gHwnd, SCENE_TEACHING_TIMER);
}
void TeachingPartInitialize() {
	int LoadGLTextures(GLuint*, TCHAR[]);
	LoadGLTextures(&Wooden_Frame, MAKEINTRESOURCE(IDBITMAP_WOODENFRAME));
	//Library DrawChars Object
	drawCharTeaching = new DrawChars();

    //new
    CordSize = (sizeof(coord_first) / sizeof(coord_first[0]));
    srand(time(NULL));

    if (CordSize > sizeRandomPoints)
    {
        for (int i = 0; i < (sizeRandomPoints - 2); i++)
        {
            randomPoints.push_back((rand() % 4000 - 2000));
        }
    }

	SetTimer(gHwnd, SCENE_TEACHING_TIMER, 85000, (TIMERPROC)MarkTeachingSceneDoneCallback);
}
void UpdateSirOutline()
{
    if (speed  < (CordSize - 3))
        speed += 2;

    //Jugad
    float sum = 0;
    for (double counter = (CordSize - speed) * 30; counter > 0; counter--)
        for (float counter1 = (CordSize - speed); counter1 > 0; counter1--)
            sum += counter1;


    if (speed < CordSize)
        first_imageCounter = (int)speed;

}

void UpdateTeachingPart(void)
{
	float translateX_margin = 0.1f;
	if (gGurukulBlendRed < 1.0f) {

		gGurukulBlendRed += 0.00035f;
		gGurukulBlendGreen -= 0.0035f;

	}
	else
		isGurukulDone = true;

	/*if (gGurukulBlendRed == 1.0f && gGurukulBlendGreen == 0.0f) {

		gGurukulBlendRed -= 0.001f;
		gGurukulBlendGreen += 0.039f;
	}*/
	if (isGurukulDone) {
		if (gMedi_X <= -3.0f) {

			gMedi_X += translateX_margin;
		}

		if (gAnatomy_X <= -1.0f) {

			gAnatomy_X += translateX_margin;
		}

		if (gPhysiology_X <= -1.0f) {

			gPhysiology_X += translateX_margin;
		}

		if (gComputer_X <= -16.0f) {

			gComputer_X += translateX_margin;
		}

		if (gUnix_X <= -14.0f) {

			gUnix_X += translateX_margin;
		}

		if (gSDK_X <= -14.0f) {

			gSDK_X += translateX_margin;
		}

		if (gSeminar_X <= -10.0f) {

			gSeminar_X += translateX_margin;
		}

		if (gFundamental_X <= -8.0f) {

			gFundamental_X += translateX_margin;
		}

		if (gOS_X <= -8.0f) {

			gOS_X += translateX_margin;
		}


		if (HideGreen >= 0.0f) {
			HideGreen = HideGreen - 0.1f;
		}

		if (gTranslateTexture <= 5.0f) {

			gTranslateTexture += translateX_margin;
		}

		if (gOpenGL_X <= -8.0f) {
			gOpenGL_X += translateX_margin;
		}
	}
	
}

void TextOnBoard() {

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glPushMatrix();
	vector<string>  textToDraw;
	if (isGurukulDone == false)
		textToDraw.resize(11);
	else
		textToDraw.resize(10);

	textToDraw[0] = "Medical";
	textToDraw[1] = "Anatomy";
	textToDraw[2] = "Physiology";
	textToDraw[3] = "Computer";
	textToDraw[4] = "Unix";
	textToDraw[5] = "Win32 SDK";
	textToDraw[6] = "Seminars";
	textToDraw[7] = "Fundamental";
	textToDraw[8] = "Multi OS";
	textToDraw[9] = "OpenGL";
	if (isGurukulDone == false)
		textToDraw[10] = "GURUKUL";


	vector<double> wordSpacVec;
	wordSpacVec.resize(11);

	wordSpacVec[0] = 0.0;
	wordSpacVec[1] = 0.0;
	wordSpacVec[2] = 0.0;
	wordSpacVec[3] = 0.0;
	wordSpacVec[4] = 0.0;
	wordSpacVec[5] = 0.5;
	wordSpacVec[6] = 0.0;
	wordSpacVec[7] = 0.0;
	wordSpacVec[8] = 0.0;
	wordSpacVec[9] = 0.0;
	wordSpacVec[10] = 0.0;


	vector<double>  textToDisplayWordSpac;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayWordSpac.push_back(wordSpacVec[i]);


	vector<double> charSpacVec;
	charSpacVec.resize(11);

	charSpacVec[0] = 1.30;
	charSpacVec[1] = 0.75;
	charSpacVec[2] = 0.75;
	charSpacVec[3] = 1.30;
	charSpacVec[4] = 0.75;
	charSpacVec[5] = 0.75;
	charSpacVec[6] = 1.30;
	charSpacVec[7] = 0.75;
	charSpacVec[8] = 0.75;
	charSpacVec[9] = 0.75;
	charSpacVec[10] = 3.0;

	vector<double>  textToDisplayCharSpac;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayCharSpac.push_back(charSpacVec[i]);


	vector<double> charWdth;
	charWdth.resize(11);

	charWdth[0] = 3.5;
	charWdth[1] = 2.5;
	charWdth[2] = 2.5;
	charWdth[3] = 3.5;
	charWdth[4] = 2.5;
	charWdth[5] = 2.5;
	charWdth[6] = 3.5;
	charWdth[7] = 2.5;
	charWdth[8] = 2.5;
	charWdth[9] = 2.5;
	charWdth[10] = 8.0;




	vector<double>  textToDisplayCharWdth;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayCharWdth.push_back(charWdth[i]);

	vector<double> traslateXParam;
	traslateXParam.resize(11);


	traslateXParam[0] = gMedi_X;
	traslateXParam[1] = gAnatomy_X;
	traslateXParam[2] = gPhysiology_X;
	traslateXParam[3] = gComputer_X;
	traslateXParam[4] = gUnix_X;
	traslateXParam[5] = gSDK_X;
	traslateXParam[6] = gSeminar_X;
	traslateXParam[7] = gFundamental_X;
	traslateXParam[8] = gOS_X;
	traslateXParam[9] = gOpenGL_X;
	traslateXParam[10] = gGurukul;


	vector<double>  textToDisplayTranslateXParam;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayTranslateXParam.push_back(traslateXParam[i]);


	vector<double> traslateYParam;
	traslateYParam.resize(11);

	traslateYParam[0] = 5.0;
	traslateYParam[1] = 3.0;
	traslateYParam[2] = 1.0;
	traslateYParam[3] = 5.0;
	traslateYParam[4] = 3.0;
	traslateYParam[5] = 1.0;
	traslateYParam[6] = -2.0;
	traslateYParam[7] = -4.0;
	traslateYParam[8] = -6.0;
	traslateYParam[9] = -8.0;
	traslateYParam[10] = -5.0;


	vector<double>  textToDisplayTranslateYParam;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayTranslateYParam.push_back(traslateYParam[i]);

	vector<double> traslateZParam;
	traslateZParam.resize(11);

	traslateZParam[0] = -30.0f;
	traslateZParam[1] = -30.0f;
	traslateZParam[2] = -30.0f;
	traslateZParam[3] = -30.0f;
	traslateZParam[4] = -30.0f;
	traslateZParam[5] = -30.0f;
	traslateZParam[6] = -30.0f;
	traslateZParam[7] = -30.0f;
	traslateZParam[8] = -30.0f;
	traslateZParam[9] = -30.0f;
	traslateZParam[10] = -29.0;


	vector<double>  textToDisplayTranslateZParam;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayTranslateZParam.push_back(traslateZParam[i]);


	vector<double> rgbVec1{ 1.0, 1.0, 0.0 };
	vector<double> rgbVec2{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec3{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec4{ 1.0, 1.0, 0.0 };
	vector<double> rgbVec5{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec6{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec7{ 1.0, 1.0, 0.0 };
	vector<double> rgbVec8{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec9{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec10{ 1.0, 1.0, 1.0 };
	vector<double> rgbVec11{ gGurukulBlendRed, gGurukulBlendGreen, 0.0 };


	vector<vector<double>>rgbValueVec;
	rgbValueVec.resize(11);

	rgbValueVec[0] = rgbVec1;
	rgbValueVec[1] = rgbVec2;
	rgbValueVec[2] = rgbVec3;
	rgbValueVec[3] = rgbVec4;
	rgbValueVec[4] = rgbVec5;
	rgbValueVec[5] = rgbVec6;
	rgbValueVec[6] = rgbVec7;
	rgbValueVec[7] = rgbVec8;
	rgbValueVec[8] = rgbVec9;
	rgbValueVec[9] = rgbVec10;
	rgbValueVec[10] = rgbVec11;

	vector<vector<double>>  textToDisplayRGBValueVec;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayRGBValueVec.push_back(rgbValueVec[i]);


	vector<double> scaleVec1{ 0.2f, 0.2f, 1.0f };
	vector<double> scaleVec2{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec3{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec4{ 0.2f, 0.2f, 1.0f };
	vector<double> scaleVec5{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec6{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec7{ 0.2f, 0.2f, 1.0f };
	vector<double> scaleVec8{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec9{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec10{ 0.1f, 0.1f, 1.0f };
	vector<double> scaleVec11{ 0.4f, 0.8f, 1.0f };


	vector<vector<double>>scaleValueVec;
	scaleValueVec.resize(11);

	scaleValueVec[0] = scaleVec1;
	scaleValueVec[1] = scaleVec2;
	scaleValueVec[2] = scaleVec3;
	scaleValueVec[3] = scaleVec4;
	scaleValueVec[4] = scaleVec5;
	scaleValueVec[5] = scaleVec6;
	scaleValueVec[6] = scaleVec7;
	scaleValueVec[7] = scaleVec8;
	scaleValueVec[8] = scaleVec9;
	scaleValueVec[9] = scaleVec10;
	scaleValueVec[10] = scaleVec11;

	vector<vector<double>>  textToDisplayScaleValueVec;
	for (int i = 0; i <= noOfLinesToDisplay; i++)
		textToDisplayScaleValueVec.push_back(scaleValueVec[i]);


	setTextToDisplayTeaching(textToDisplayWordSpac, textToDisplayCharSpac, textToDisplayCharWdth, textToDisplayTranslateXParam, textToDisplayTranslateYParam, textToDisplayTranslateZParam, textToDisplayRGBValueVec, textToDisplayScaleValueVec, textToDraw);

	drawCharTeaching->setWordToDisplayOnScreen();

	glPopMatrix();
}


void displayTeaching(void)
{

	void HideTextureCube(void);
	void board(void);
	void UpdateTeachingPart(void);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	DisplaySirOutline();
	if (isOutlineDone){
		board();
		TextOnBoard();
		UpdateTeachingPart();
	}

	
}



void board(void) {


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-3.8f, 0.0f, -30.1f);

	glScalef(17.9f, 12.0f, 0.0f);

	glRotatef(0.0f, 0.0f, 1.0f, 0.0f);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, Wooden_Frame);


	glBegin(GL_QUADS);

	//Upper Pole

	//Front face
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, 0.71f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, 0.71f, 1.0f);

	//Back face	
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, 0.73f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, 0.73f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.79f, 0.7f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, 0.7f, 0.5f);


	//Top face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.79f, 0.73f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, 0.73f, 0.5f);

	//Bottom face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.71f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, 0.71f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.79f, 0.7f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, 0.7f, 0.5f);


	//Right face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.79f, 0.73f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.79f, 0.7f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, 0.71f, 1.0f);


	//Left face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.79f, 0.73f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, 0.74f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, 0.71f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.79f, 0.7f, 0.5f);


	//LEFT Pole

	//Front face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.77f, -0.73f, 1.0f);

	//Back face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.76f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.76f, -0.72f, 0.5f);

	//Top face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.76f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, 0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.79f, 0.72f, 0.5f);

	//Bottom face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.76f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.77f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);

	//Left face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, 0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);

	//Right face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.77f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.76f, -0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.76f, 0.72f, 0.5f);


	//Bottom Pole
	//Front face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, -0.7f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.8f, -0.7f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);

	//Back face	
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);


	//Top face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.7f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, -0.7f, 1.0f);


	//Bottom face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);


	//Right face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, -0.7f, 1.0f);


	//Left face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.79f, -0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.79f, -0.69f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.8f, -0.7f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.8f, -0.73f, 1.0f);

	//Right Pole

	//Front face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.77f, -0.73f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);

	//Back face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.79f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.76f, 0.72f, 0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.76f, -0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);

	//Top face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.76f, 0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, 0.72f, 0.5f);


	//Bottom face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.77f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.76f, -0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);


	//Left face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.8f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.8f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.79f, 0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.79f, -0.72f, 0.5f);

	//Right face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.77f, -0.73f, 1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.77f, 0.73f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.76f, 0.72f, 0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.76f, -0.72f, 0.5f);

	glEnd();
	glDisable(GL_TEXTURE_2D);



	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Wooden_Frame);

	glBegin(GL_QUADS);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.45f, -0.73f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.47f, -0.73f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.58f, -1.0f, 0.0f);

	glEnd();
	glDisable(GL_TEXTURE_2D);



	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Wooden_Frame);

	glBegin(GL_QUADS);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.45f, -0.73f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.47f, -0.73f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.6f, -1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.58f, -1.0f, 0.0f);

	glEnd();
	glDisable(GL_TEXTURE_2D);


	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.392f, 0.0f);

	glVertex3f(-0.77f, 0.7f, 0.0f);
	glVertex3f(0.77f, 0.7f, 0.0f);
	glVertex3f(0.77f, -0.7f, 0.0f);
	glVertex3f(-0.77f, -0.7f, 0.0f);

	glEnd();
}

void uninitializeTeaching() {
	delete drawCharTeaching;
}