#pragma once
#include "Computer.h"
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>


//global function declarations
int LoadGLTextures_Computer(GLuint *, TCHAR[]);


float angleCube = 0.0f;
float xScale = 4.0f;
float yScale = 4.0f;
float zScale = 4.0f;

float xScale_SecondTime = 1.0f;
float yScale_SecondTime = 1.0f;
float zScale_SecondTime = 1.0f;

bool scaleComplitionFlag = false;
bool rotateComplitionFlag = false;
bool scene_MonitorComplitionFlag = false;

void update_Computer();

GLuint Texture_Kundali;

void scene_MonitorInitialize(void)
{
	LoadGLTextures_Computer(&Texture_Kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));
    glBindTexture(GL_TEXTURE_2D, Texture_Kundali);
}


void scene_MonitorDisplay(void)
{
	//function prototypes
    void drawMonitor(void);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    glTranslatef(0.05f, 0.43f, -8.0f);
    glScalef(xScale, yScale, zScale);

    if (xScale <= 1.4f)
    {
        scaleComplitionFlag = true;
    }

    if (scaleComplitionFlag == true)
        glRotatef(angleCube, 0.0f, 1.0f, 0.0f);

    if (angleCube >= 360.0f)
    {
        rotateComplitionFlag = true;
    }

    if (rotateComplitionFlag == true)
    {
        glScalef(xScale_SecondTime, yScale_SecondTime, zScale_SecondTime);
    }

    drawMonitor();
	update_Computer();
}

void drawMonitor(void)
{
	
	//drawing of monitor frame
	//top side of frame
	glBegin(GL_QUADS);
	//front face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.55f, 1.0f);
	glVertex3f(1.0f, 0.55f, 1.0f);
	//top face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	//left face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.55f, 0.9f);
	glVertex3f(-1.0f, 0.55f, 1.0f);
	//right face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(1.0f, 0.55f, 1.0f);
	glVertex3f(1.0f, 0.55f, 0.9f);
	//bottom face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	//back face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.55f, 0.9f);
	glVertex3f(1.0f, 0.55f, 0.9f);
	glEnd();


	//left side of frame
	glBegin(GL_QUADS);
	//front face
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-0.85f, 0.7f, 1.0f);
	glVertex3f(-0.85f, -0.7f, 1.0f);
	glVertex3f(-1.0f, -0.7f, 1.0f);
	//left face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 1.0f);
	//right face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-0.85f, 0.7f, 0.9f);
	glVertex3f(-0.85f, 0.7f, 1.0f);
	glVertex3f(-0.85f, -0.7f, 1.0f);
	glVertex3f(-0.85f, -0.7f, 0.9f);
	//top face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-0.85f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-0.85f, 0.7f, 1.0f);
	//bottom face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-0.85f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 1.0f);
	glVertex3f(-0.85f, -0.7f, 1.0f);
	glEnd();


	//bottom side of frame
	glBegin(GL_QUADS);
	//front face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, -0.7f, 1.0f);
	glVertex3f(-1.0f, -0.7f, 1.0f);
	glVertex3f(-1.0f, -0.85, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	//left face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-1.0f, -0.7f, 1.0f);
	glVertex3f(-1.0f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	//right face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	glVertex3f(1.0f, -0.7f, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	//bottom face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	//back face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	glEnd();


	//right side of frame
	glBegin(GL_QUADS);
	//front face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(0.85f, 0.7f, 1.0f);
	glVertex3f(0.85f, -0.7f, 1.0f);
	glVertex3f(1.0f, -0.7f, 1.0f);
	//left face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.85f, 0.7f, 1.0f);
	glVertex3f(0.85f, 0.7f, 0.9f);
	glVertex3f(0.85f, -0.7f, 0.9f);
	glVertex3f(0.85f, -0.7f, 1.0f);
	//right face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(1.0f, -0.7f, 1.0f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	//top face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(0.85f, 0.7f, 0.9f);
	glVertex3f(0.85f, 0.7f, 1.0f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	//bottom face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	glVertex3f(0.85f, -0.7f, 0.9f);
	glVertex3f(0.85f, -0.7f, 1.0f);
	glVertex3f(1.0f, -0.7f, 1.0f);
	//back face
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(0.85f, 0.7f, 0.9f);
	glVertex3f(0.85f, -0.7f, 0.9f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	glEnd();
	//here finishes drawing of monitor frame

	
	
	//drawing of monitor

	glBegin(GL_QUADS);
	//front side on monitor
	glColor3f(0.15f, 0.15f, 0.15f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, -0.7f, 0.9f);
	glVertex3f(1.0f, -0.7f, 0.9f);
	//left side on monitor
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	//right side of monitor
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	//top side of monitor
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	//bottom side of monitor
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	//back side of monitor
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	glEnd();

	
	//drawing small box which is located at back side of monitor
	
	glBegin(GL_QUADS);
	//left side of small box
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(-0.4f, 0.25f, 0.1f);
	glVertex3f(-0.4f, 0.25f, -0.4);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(-0.4f, -0.55, 0.1f);
	//right side of small box
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glVertex3f(0.4f, 0.25f, 0.1f);
	glVertex3f(0.4f, -0.55f, 0.1f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	//top side of small box
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, 0.25f, 0.1f);
	glVertex3f(0.4f, 0.25f, 0.1f);
	//bottom side of small box
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(-0.4f, -0.55f, 0.1f);
	glVertex3f(0.4f, -0.55f, 0.1f);
	//back side of small box
	glColor3f(0.75f, 0.75f, 0.75f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	glEnd();



    //applying kundali texture to monitor screen
 //   glEnable(GL_LIGHT0);
  //  glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(0.83f, 0.57f, 0.91f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-0.83, 0.57f, 0.91f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-0.83, -0.68f, 0.91f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(0.83, -0.68f, 0.91f);
    glEnd();
    glDisable(GL_TEXTURE_2D);
   // glDisable(GL_LIGHTING);

	//drawing monitor border
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	//front frame
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	glVertex3f(-1.0f, -0.85, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, 0.7f, 1.0f);

	//border for other area of monitor
	glVertex3f(-1.0f, 0.7f, 1.0f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-1.0f, 0.7f, 0.9f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 1.0f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(-1.0f, -0.85f, 0.9f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(1.0f, 0.7f, 1.0f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(1.0f, 0.7f, 0.9f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(1.0f, -0.85f, 1.0f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	glVertex3f(1.0f, -0.85f, 0.9f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, 0.3f, 0.1f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(-0.5f, -0.7f, 0.1f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	glVertex3f(0.5f, -0.7f, 0.1f);
	glVertex3f(0.5f, 0.3f, 0.1f);
	glVertex3f(0.4f, 0.25f, 0.1f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4, 0.25, 0.1f);
	glVertex3f(-0.4f, 0.25, -0.4f);
	glVertex3f(-0.4, -0.55f, 0.1f);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, -0.55f, 0.1f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, 0.25f, -0.4f);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(-0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, -0.55f, -0.4f);
	glVertex3f(0.4f, 0.25f, -0.4f);
	glEnd();
}

void update_Computer(void)
{
    if (xScale >= 1.2f)
    {
        xScale = xScale - 0.0005f;
        yScale = yScale - 0.0005f;
        zScale = zScale - 0.0005f;
    }

    if (xScale <= 1.2f)
    {
        scaleComplitionFlag = true;
    }

    if (scaleComplitionFlag == true)

    {
        if (angleCube <= 360.0f)
            angleCube = angleCube + 0.017f;
    }

    if (angleCube >= 360.0f)
    {
        rotateComplitionFlag = true;
    }

    if (rotateComplitionFlag == true)
    {
        if (xScale_SecondTime <= 8.0f)
        {
            xScale_SecondTime = xScale_SecondTime + 0.0002f;
            yScale_SecondTime = yScale_SecondTime + 0.0002f;
            zScale_SecondTime = zScale_SecondTime + 0.0002f;
        }

        if (xScale_SecondTime >= 3.0 && xScale_SecondTime <= 8.0f)
        {
            xScale_SecondTime = xScale_SecondTime + 0.01f;
            yScale_SecondTime = yScale_SecondTime + 0.01f;
            zScale_SecondTime = zScale_SecondTime + 0.01f;
        }

        if (xScale_SecondTime >= 8.0)
            scene_MonitorComplitionFlag = true;
    }
}


int LoadGLTextures_Computer(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		//generate mipmapped texture (3 bytes, width, height & data from bmp)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}
