#pragma once
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DrawChars.h"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>

void displayCompilerTable(void);
void initializeCompilerTable(void);
void updateCompilerTableScale(void);
void updateCompilerTable(void);
void drawBlueScreen(void);
void drawCompilerErrorBox(void);
void drawErrorBox(void);
void uninitializeCompilerTable();

