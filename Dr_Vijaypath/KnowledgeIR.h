#pragma once
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DrawChars.h"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>
using namespace std;


/*End of Global variables for Gurukul board*/
void knowledgeIRUpdate();
DWORD WINAPI PlayKeyBoardStrokeSoundCallBack(_In_ LPVOID lpParameter);
void knowledgeIR_initialize();
void knowledgeIR_uninitialize();
void drawSirWhiteBkTexture(void);
void knowledgeIRdisplay(void);
void knowledgeIRUpdate(void);
void setTextToDisplayKnowledgeIR(vector<double>& wordSpacVec, vector<double>& charSpacVec, vector<double> charWdth, vector<double>& traslateXParam, vector<double>& traslateYParam, vector<double>& traslateZParam, vector<vector<double>>& rgbValueVec, vector<vector<double>>& scaleValueVec, vector<string>&  textToDraw);